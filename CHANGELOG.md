## 9.0.0 (September 10, 2020)


## 2.1.0 (April 22, 2019)
  - Update dependencies, but there is no need to rebuild packages
  - ngx-net-crud published
  - Clean up
  - Fix public api for ngx-net-crud
  - Fix import path in ngx-net-crud
  - Configure ngx-net-crud package
  - Deployment repo for ngx-net-crud
  - Scripts for ngx-net-crud
  - ngx-mvc published
  - Fix package pathes in ngx-mvc
  - Configure ngx-mvc package
  - Deployment repo for ngx-mvc
  - Scripts for ngx-mvc
  - ngx-noty published
  - Configure ngx-noty package.json
  - Deployment repo for ngx-noty
  - Scripts for ngx-noty
  - k
  - Introduce the module for ngx-pipes
  - Set ngx-pipes version
  - Deployment repo for ngx-pipes
  - Scripts for ngx-pipes
  - Submodule
  - ngx-spinner dependencies
  - Set ngx-spinner version
  - Deployment repo for ngx-spinner
  - Scripts for ngx-spinner
  - submodule
  - Set ngx-rxjs version
  - Deployment repo for ngx-rxjs
  - Scripts for ngx-rxjs
  - Submodule
  - Set ngx-model version
  - Deployment repo for ngx-model
  - Scripts for ngx-model
  - submodule
  - Set ngx-img-loader version to be 7.0
  - Deployment repo for ngx-img-loader
  - Scripts for ngx-img-loader
  - ngx-i18n
  - Fix ngx-18n package
  - Fix the import path for ngx-i18n
  - Deployment repo for ngx-i18n
  - Scripts for ngx-i18n deployment
  - ngx-events
  - Fix scripts for ngx-events
  - Renaming spec
  - Fix package path in ngx-events test files
  - Update ngx-events revision to 7.0.0
  - Move spec to lib
  - Update packages
  - Improve makefile scripts
  - Deployment flow and scripts in Makefile
  - Set up the deployment repo for ngx-events
  - Fix ngx-i18n
  - Configure baseUrl as it is required for builiding a lib
  - Move crud services into ngx-net-crud
  - Introduce ngx-net-crud
  - Fix public api for ngx-model
  - Move base entity into ngx-model
  - Introduce ngx-model
  - Fix public api for ngx-rxjs
  - Move observable extension into ngx-rxjs
  - Introduce ngx-rxjs
  - Fix public api for ngx-img-loader
  - Move lazy load image into ngx-img-loader
  - Introduce ngx-img-loader
  - Fix public api for ngx-i18n
  - Renaming continues
  - Renaming ngx-i18 to ngx-i18n
  - Move resource loader service into ngx-i18n
  - Introduce ngx-i18
  - Fix public api for ngx-pipes
  - Move pipes into ngx-pipes
  - Introduce ngx-pipes
  - Fix exported interfaces for ngx-events
  - Move events into ngx-events lib
  - Setup ngx-events lib
  - ngx-noty lib
  - Factor out noty into a standalone library
  - Set up ngx-spinner lib structure
  - Fix import statements for ngx-spinner
  - Fix export for ngx-mvc
  - Factor spinner to ngx-spinner
  - We on purpose include indicator interface to make ngx-mvc lib to be self contained
  - Fix import statements in ngx-mvc lib
  - ngx-mvc lib setup
  - Move mvc into a standalone library
  - Refine angular metadata
  - Explicit dependencies
  - Upgrade to Angular 7.0
  - Update package
  - Fix bugs
  - Generalize observer crub to decouple the server interface from internal data
  - Fix a bug: httpParams is value semantics
  - Improvement: Make buildMediator be a promise to support more general cases
  - Improve backbone-based page list to support the use of cache
  - pdf
  - Document backbone backed page
  - Dependencies for doc
  - Doc page life cycle
  - Document page life cyle
  - Renaming
  - Renaming
  - Merge branch 'master' of https://bitbucket.org/polpware/polpware-ngx-components
  - dependency packages for latex
  - Switch to use rxjs-based mediator
  - Fix bugs on detecting the current state of the spinner
  - Fix a bug on crub service
  - Fix a syntax error
  - Extend getListAsync method to support query parameters in observerable-crud
  - Fix package.json syntax
  - Init impl for spinner service
  - Remove tslint.json in this project, as it causes some trouble in emacs
  - tslint
  - Normalize package dependency versions in package.json
  - Update tslint
  - Introduce road map to evolution and upgrades
  - Clean up

## 2.0.0 (October 10, 2018)


## 0.1.0 (October 10, 2018)
  - Start to bump to 2.1.0-dev for new features and bugs
  - Specify necessary dependency in package.json
  - Improved loading indicator: now building loading indicator upon a general spinner service
  - Renaming
  - Improve mvc page implementation: Now implements ILoadingIndicator and other indicators
  - Improve INotyOperations interface
  - Introduce a set of noty operations and a service for these operations
  - Improve crub service; abstracting out the list/delete/create/update Urls into methods
  - Drop the use of outdated ng2-component-spinner; use an abstract spinner service instead
  - Extend loadingIndicator interface
  - Factor out the requirements for refreshing, loading, and more loading indicators as interfaces
  - Merge branch 'master' of https://bitbucket.org/polpware/polpware-ngx-components
  - Remove redundant code; clean
  - Improvement: Scheduler of the sliding cache now running outside Angular
  - Generalize platform list page to allow turnOnMediator to be passed into additional params
  - Factored onNewItemsReady in ngstore-backed-list-page
  - Fix a bug: ngstore list page needs to invoke onItemsReady
  - Fix a bug: Subscription might have been closed
  - Improve ngstore backed list page to expose media options to derived classes
  - Fix a bug: toPromise did NOT release subscription on success or failure
  - Extension: onDocumentReady passes inputs to ensureDataProvider
  - Fix bugs: segments pipe
  - New fature: A pipe of computing the segments of a string; the extended version of the string split
  - Fix a bug
  - Fix bugs
  - Fix errors
  - k
  - Clean
  - fix bugs
  - Refactor...
  - Refactoring ...
  - Refactoring ...
  - Clean ...
  - Refactoring code: instead of using a separate library, we use the app as the host of our lib
  - Lint
  - k
  - Use angular built-in lib to manage src
  - Rename
  - packages
  - Upgrading ...
  - Rename project into polpware-ngx-components
  - Extension: Allow to pass a callback on broadcasting an event
  - unzip object
  - unzip object pipe
  - Fix a bug: resource-loader ctor
  - Enhancement: Generalize the interface on manipulating cache
  - Enhancement: Generalize input params for a few methods
  - list page
  - Merge branch 'master' of https://bitbucket.org/polpware/principleware-ngx-components
  - Fix resource loader service
  - Merge branch 'master' of https://bitbucket.org/polpware/principleware-ngx-components
  - Resource loader service
  - Revert back to use dist under spinner component
  - Enhancement: Generalize crud service (more fine control on request and respone handling
  - Access modifier for crud service
  - Remove showRefresher
  - merge
  - Refine the interfaces
  - Clarify that id is required at client side, no matter whether the server side uses id or Id
  - fix typo
  - To disambiguate id and Id
  - New feature: refresher
  - k
  - gulpfile
  - Configure tsconfig
  - Introduce interfaces
  - Minor
  - doc
  - More deps
  - New deps for doc generation
  - Renaming
  - refactoring
  - Define two school of class methods: async and sync
  - Enhance crud service: Pass a specific spinner in each public method
  - Enhancement on Spinner service setting and usage
  - minor improvement
  - observerable table services
  - Use updateUrl instead
  - New feature: crud operations
  - New feature: Support refreshing indicator
  - New feature: A pipe for computing the parent directory of a given path
  - mvc
  - more pipes
  - to promise
  - lazy load image
  - unroll array pipe
  - k
  - services
  - mvc
  - k
  - k
  - more
  - k
  - more doc
  - placeholder for image folder; required by org mode
  - design on list page
  - Renaming
  - init commit

