NgxI18nStoreBuildDist := ./dist/polpware/ngx-i18n-store
NgxI18nStoreDeployTarget := ./deployment/polpware-ngx-i18n-store

build-ngx-i18n-store:
	echo "Build ..."
	ng build @polpware/ngx-i18n-store
	echo "Build done"

copy-ngx-i18n-store:
	echo "Clean old files ..."
	cd $(NgxI18nStoreDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxI18nStoreBuildDist)/* $(NgxI18nStoreDeployTarget)/
	echo "Copy files done"

push-ngx-i18n-store:
	echo "Find new files ..."
	cd $(NgxI18nStoreDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxI18nStoreDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxI18nStoreDeployTarget) && git push
	echo "Push done"

deploy-ngx-i18n-store: build-ngx-i18n-store copy-ngx-i18n-store push-ngx-i18n-store



.PHONY: build-ngx-i18n-store copy-ngx-i18n-store push-ngx-i18n-store deploy-ngx-i18n-store
