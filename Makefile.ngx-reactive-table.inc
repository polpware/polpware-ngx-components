NgxReactiveTableBuildDist := ./dist/polpware/ngx-reactive-table
NgxReactiveTableDeployTarget := ./deployment/polpware-ngx-reactive-table

build-ngx-reactive-table:
	echo "Build ..."
	ng build @polpware/ngx-reactive-table
	echo "Build done"

copy-ngx-reactive-table:
	echo "Clean old files ..."
	cd $(NgxReactiveTableDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxReactiveTableBuildDist)/* $(NgxReactiveTableDeployTarget)/
	echo "Copy files done"

push-ngx-reactive-table:
	echo "Find new files ..."
	cd $(NgxReactiveTableDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxReactiveTableDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxReactiveTableDeployTarget) && git push
	echo "Push done"

deploy-ngx-reactive-table: build-ngx-reactive-table copy-ngx-reactive-table push-ngx-reactive-table



.PHONY: build-ngx-reactive-table copy-ngx-reactive-table push-ngx-reactive-table deploy-ngx-reactive-table
