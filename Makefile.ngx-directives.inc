NgxDirectivesBuildDist := ./dist/polpware/ngx-directives
NgxDirectivesDeployTarget := ./deployment/polpware-ngx-directives

build-ngx-directives:
	echo "Build ..."
	ng build @polpware/ngx-directives
	echo "Build done"

copy-ngx-directives:
	echo "Clean old files ..."
	cd $(NgxDirectivesDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxDirectivesBuildDist)/* $(NgxDirectivesDeployTarget)/
	echo "Copy files done"

push-ngx-directives:
	echo "Find new files ..."
	cd $(NgxDirectivesDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxDirectivesDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxDirectivesDeployTarget) && git push
	echo "Push done"

deploy-ngx-directives: build-ngx-directives copy-ngx-directives push-ngx-directives



.PHONY: build-ngx-directives copy-ngx-directives push-ngx-directives deploy-ngx-directives
