NgxEventsBuildDist := ./dist/polpware/ngx-events
NgxEventsDeployTarget := ./deployment/polpware-ngx-events

build-ngx-events:
	echo "Build ..."
	ng build @polpware/ngx-events
	echo "Build done"

copy-ngx-events:
	echo "Clean old files ..."
	cd $(NgxEventsDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxEventsBuildDist)/* $(NgxEventsDeployTarget)/
	echo "Copy files done"

push-ngx-events:
	echo "Find new files ..."
	cd $(NgxEventsDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxEventsDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxEventsDeployTarget) && git push
	echo "Push done"

deploy-ngx-events: build-ngx-events copy-ngx-events push-ngx-events



.PHONY: build-ngx-events copy-ngx-events push-ngx-events deploy-ngx-events
