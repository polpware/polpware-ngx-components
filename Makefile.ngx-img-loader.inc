NgxImgLoaderBuildDist := ./dist/polpware/ngx-img-loader
NgxImgLoaderDeployTarget := ./deployment/polpware-ngx-img-loader

build-ngx-img-loader:
	echo "Build ..."
	ng build @polpware/ngx-img-loader
	echo "Build done"

copy-ngx-img-loader:
	echo "Clean old files ..."
	cd $(NgxImgLoaderDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxImgLoaderBuildDist)/* $(NgxImgLoaderDeployTarget)/
	echo "Copy files done"

push-ngx-img-loader:
	echo "Find new files ..."
	cd $(NgxImgLoaderDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxImgLoaderDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxImgLoaderDeployTarget) && git push
	echo "Push done"

deploy-ngx-img-loader: build-ngx-img-loader copy-ngx-img-loader push-ngx-img-loader



.PHONY: build-ngx-i18n copy-ngx-i18n push-ngx-i18n deploy-ngx-i18n
