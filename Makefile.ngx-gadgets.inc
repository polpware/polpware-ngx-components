NgxGadgetsBuildDist := ./dist/polpware/ngx-gadgets
NgxGadgetsDeployTarget := ./deployment/polpware-ngx-gadgets

build-ngx-gadgets:
	echo "Build ..."
	ng build @polpware/ngx-gadgets
	echo "Build done"

copy-ngx-gadgets:
	echo "Clean old files ..."
	cd $(NgxGadgetsDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxGadgetsBuildDist)/* $(NgxGadgetsDeployTarget)/
	echo "Copy files done"

push-ngx-gadgets:
	echo "Find new files ..."
	cd $(NgxGadgetsDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxGadgetsDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxGadgetsDeployTarget) && git push
	echo "Push done"

deploy-ngx-gadgets: build-ngx-gadgets copy-ngx-gadgets push-ngx-gadgets



.PHONY: build-ngx-gadgets copy-ngx-gadgets push-ngx-gadgets deploy-ngx-gadgets
