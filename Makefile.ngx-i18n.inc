NgxI18nBuildDist := ./dist/polpware/ngx-i18n
NgxI18nDeployTarget := ./deployment/polpware-ngx-i18n

build-ngx-i18n:
	echo "Build ..."
	ng build @polpware/ngx-i18n
	echo "Build done"

copy-ngx-i18n:
	echo "Clean old files ..."
	cd $(NgxI18nDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxI18nBuildDist)/* $(NgxI18nDeployTarget)/
	echo "Copy files done"

push-ngx-i18n:
	echo "Find new files ..."
	cd $(NgxI18nDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxI18nDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxI18nDeployTarget) && git push
	echo "Push done"

deploy-ngx-i18n: build-ngx-i18n copy-ngx-i18n push-ngx-i18n



.PHONY: build-ngx-i18n copy-ngx-i18n push-ngx-i18n deploy-ngx-i18n
