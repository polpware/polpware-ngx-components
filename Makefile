
# ngx-events

include Makefile.ngx-events.inc

# ngx-i18n

include Makefile.ngx-i18n.inc

include Makefile.ngx-i18n-store.inc

# ngx-img-loader

include Makefile.ngx-img-loader.inc

# ngx-events

include Makefile.ngx-model.inc

# ngx-rxjs

include Makefile.ngx-rxjs.inc

# ngx-spinner

include Makefile.ngx-spinner.inc

# ngx-pipes

include Makefile.ngx-pipes.inc

# ngx-noty

include Makefile.ngx-noty.inc

# ngx-mvc

include Makefile.ngx-mvc.inc

# ngx-net-crud

include Makefile.ngx-net-crud.inc

# ngx-directives

include Makefile.ngx-directives.inc

# ngx-logger
include Makefile.ngx-logger.inc

# ngx-wizard
include Makefile.ngx-wizard.inc

# ngx-reactive-table
include Makefile.ngx-reactive-table.inc

# ngx-gadgets
include Makefile.ngx-gadgets.inc

# ngx-alert
include Makefile.ngx-alert.inc


build-all: build-ngx-events build-ngx-i18n build-ngx-model build-ngx-rxjs build-ngx-spinner \
 build-ngx-pipes build-ngx-noty build-ngx-mvc build-ngx-directives build-ng-logger build-ngx-wizard \
 build-ngx-reactive-table build-ngx-gadgets build-ngx-alert
	echo 'hello'

.PHONY: build-all
