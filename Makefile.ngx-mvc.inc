NgxMvcBuildDist := ./dist/polpware/ngx-mvc
NgxMvcDeployTarget := ./deployment/polpware-ngx-mvc

build-ngx-mvc:
	echo "Build ..."
	ng build @polpware/ngx-mvc
	echo "Build done"

copy-ngx-mvc:
	echo "Clean old files ..."
	cd $(NgxMvcDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxMvcBuildDist)/* $(NgxMvcDeployTarget)/
	echo "Copy files done"

push-ngx-mvc:
	echo "Find new files ..."
	cd $(NgxMvcDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxMvcDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxMvcDeployTarget) && git push
	echo "Push done"

deploy-ngx-mvc: build-ngx-mvc copy-ngx-mvc push-ngx-mvc



.PHONY: build-ngx-mvc copy-ngx-mvc push-ngx-mvc deploy-ngx-mvc
