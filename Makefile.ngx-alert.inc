NgxAlertBuildDist := ./dist/polpware/ngx-alert
NgxAlertDeployTarget := ./deployment/polpware-ngx-alert

build-ngx-alert:
	echo "Build ..."
	ng build @polpware/ngx-alert
	echo "Build done"

copy-ngx-alert:
	echo "Clean old files ..."
	cd $(NgxAlertDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxAlertBuildDist)/* $(NgxAlertDeployTarget)/
	echo "Copy files done"

push-ngx-alert:
	echo "Find new files ..."
	cd $(NgxAlertDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxAlertDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxAlertDeployTarget) && git push
	echo "Push done"

deploy-ngx-alert: build-ngx-alert copy-ngx-alert push-ngx-alert



.PHONY: build-ngx-alert copy-ngx-alert push-ngx-alert deploy-ngx-alert
