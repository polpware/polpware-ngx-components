NgxModelBuildDist := ./dist/polpware/ngx-model
NgxModelDeployTarget := ./deployment/polpware-ngx-model

build-ngx-model:
	echo "Build ..."
	ng build @polpware/ngx-model
	echo "Build done"

copy-ngx-model:
	echo "Clean old files ..."
	cd $(NgxModelDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxModelBuildDist)/* $(NgxModelDeployTarget)/
	echo "Copy files done"

push-ngx-model:
	echo "Find new files ..."
	cd $(NgxModelDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxModelDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxModelDeployTarget) && git push
	echo "Push done"

deploy-ngx-model: build-ngx-model copy-ngx-model push-ngx-model



.PHONY: build-ngx-model copy-ngx-model push-ngx-model deploy-ngx-model
