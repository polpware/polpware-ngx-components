NgxWizardBuildDist := ./dist/polpware/ngx-wizard
NgxWizardDeployTarget := ./deployment/polpware-ngx-wizard

build-ngx-wizard:
	echo "Build ..."
	ng build @polpware/ngx-wizard
	echo "Build done"

copy-ngx-wizard:
	echo "Clean old files ..."
	cd $(NgxWizardDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxWizardBuildDist)/* $(NgxWizardDeployTarget)/
	echo "Copy files done"

push-ngx-wizard:
	echo "Find new files ..."
	cd $(NgxWizardDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxWizardDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxWizardDeployTarget) && git push
	echo "Push done"

deploy-ngx-wizard: build-ngx-wizard copy-ngx-wizard push-ngx-wizard



.PHONY: build-ngx-wizard copy-ngx-wizard push-ngx-wizard deploy-ngx-wizard
