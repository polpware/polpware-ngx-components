NgxPipesBuildDist := ./dist/polpware/ngx-pipes
NgxPipesDeployTarget := ./deployment/polpware-ngx-pipes

build-ngx-pipes:
	echo "Build ..."
	ng build @polpware/ngx-pipes
	echo "Build done"

copy-ngx-pipes:
	echo "Clean old files ..."
	cd $(NgxPipesDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxPipesBuildDist)/* $(NgxPipesDeployTarget)/
	echo "Copy files done"

push-ngx-pipes:
	echo "Find new files ..."
	cd $(NgxPipesDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxPipesDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxPipesDeployTarget) && git push
	echo "Push done"

deploy-ngx-pipes: build-ngx-pipes copy-ngx-pipes push-ngx-pipes



.PHONY: build-ngx-pipes copy-ngx-pipes push-ngx-pipes deploy-ngx-pipes
