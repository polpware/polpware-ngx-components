NgxRxjsBuildDist := ./dist/polpware/ngx-rxjs
NgxRxjsDeployTarget := ./deployment/polpware-ngx-rxjs

build-ngx-rxjs:
	echo "Build ..."
	ng build @polpware/ngx-rxjs
	echo "Build done"

copy-ngx-rxjs:
	echo "Clean old files ..."
	cd $(NgxRxjsDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxRxjsBuildDist)/* $(NgxRxjsDeployTarget)/
	echo "Copy files done"

push-ngx-rxjs:
	echo "Find new files ..."
	cd $(NgxRxjsDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxRxjsDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxRxjsDeployTarget) && git push
	echo "Push done"

deploy-ngx-rxjs: build-ngx-rxjs copy-ngx-rxjs push-ngx-rxjs



.PHONY: build-ngx-rxjs copy-ngx-rxjs push-ngx-rxjs deploy-ngx-rxjs
