#+TITLE:     Page Life Cycle
#+AUTHOR:    Tom Tang
#+EMAIL:     principleware@gmail.com
#+DATE:      \today
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LATEX_CLASS: article
#+LATEX_HEADER: \usepackage{graphics}
#+LATEX_HEADER: \usepackage{sectsty,textcase}
#+LATEX_HEADER: \usepackage{algorithm}
#+LATEX_HEADER: \usepackage{algpseudocode}
#+LATEX_HEADER: \usepackage{TikZ-UML}
#+LATEX_HEADER: \usepackage{syntax}
#+LATEX_HEADER: \usepackage{listings}
#+LINK_UP:   
#+LINK_HOME: 
#+XSLT:

* References

\begin{thebibliography}{999}

\bibitem{list-page-design}
  Tom Tang,
  \emph{Design for List Page}.
  in-house, polpware-ngx-components.

\end{thebibliography}
  

* Introduction

This document extends the list page in \cite{list-page-design}, to
introduce page life cycle events and necessary indicators.

* Architecture

There are two important stages for a page. One is when the page is
ready for conducting the page-specfic logic to load data and render
it. This stage is called \emph{onDocumentReady}; it expects any number
of arguments. Conversly, the moment when the page is ready for being
destoryed is the other stage. We name it as \emph{onDocumentDestroy},
and similarly, it takes any number of arguments.

** onDocumentReady

#+BEGIN_SRC dot :file images/on-doc-ready.png :exports results
  digraph G {
     node [shape="box"];
     compound=true;
     label="onDocumentReady";

     subgraph cluster2 {
         label="ensureDataProvider";
         n1 [label="buildMediator"];         
         n2 [label="turnOnMediator"];
         n3 [label="afterMediatorOn"];
     }

     n1 -> n2;
     n2 -> n3;
  }
#+END_SRC

#+CAPTION: The suggested logic on the \emph{onDocumentReady} event
#+ATTR_LATEX: :width 0.4\textwidth
#+RESULTS:
[[file:images/on-doc-ready.png]]


** onDocumentDestroy

#+BEGIN_SRC dot :file images/on-doc-destroy.png :exports results
  digraph G {
     node [shape="box"];
     compound=true;
     label="onDocumentDestroy";

     subgraph cluster2 {
         label="ensureDataProvider";
         n2 [label="turnOffMediator"];
         n3 [label="afterMediatorOff"];
     }

     n2 -> n3;
  }
#+END_SRC

#+CAPTION: The suggested logic on the \emph{onDocumentDestroy} event
#+ATTR_LATEX: :width 0.4\textwidth
#+RESULTS:
[[file:images/on-doc-destroy.png]]


* Advanced features 

** Cache

\begin{figure}
\centering
\begin{tikzpicture}
\umlinterface{ICacheFeature} {
   readMediatorFromCache(key: string): IListMediatorPublic \\
   writeMediatorIntoCache(key: string, value: IListMediatorPublic): void \\
   addOnCacheExpireHandler(key: string): void \\
   removeOnCacheExpireHandler(key: string): void
}{}
\end{tikzpicture}
\caption{Cache Interface}
\label{fig:cache-feature}
\end{figure}

** Indicators 

\begin{figure}
\centering
\begin{tikzpicture}
\umlinterface{IIndicatorFeature} {
    showLoadingIndicator(...args: any[]) \\
    hideLoadingIndicator(...args: any[]) \\
    setLoadingIndicatorDelay(seconds: number) \\
    showMoreLoading(...args: any[]) \\
    hideMoreLoading(...args: any[]) \\
    showRefreshingIndicator(...args: any[]) \\
    hideRefreshingIndicator(...args: any[]) 
}{}
\end{tikzpicture}
\caption{Indicator Interface}
\label{fig:indicator-feature}
\end{figure}


