#+TITLE:     Design for List Page
#+AUTHOR:    Tom Tang
#+EMAIL:     principleware@gmail.com
#+DATE:      \today
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LATEX_CLASS: article
#+LATEX_HEADER: \usepackage{graphics}
#+LATEX_HEADER: \usepackage{sectsty,textcase}
#+LATEX_HEADER: \usepackage{algorithm}
#+LATEX_HEADER: \usepackage{algpseudocode}
#+LATEX_HEADER: \usepackage{TikZ-UML}
#+LATEX_HEADER: \usepackage{syntax}
#+LATEX_HEADER: \usepackage{listings}
#+LINK_UP:   
#+LINK_HOME: 
#+XSLT:


* Introduction 

A list page is used to render a list of items, and optionally support
such features as /pull-to-refresh/ and /infinite-loading/. The
/pull-to-refresh/ feature allows users to refresh the page content,
usually triggered by the event when users are pulling down the page;
the /infinite-loading/ allows users to reveal more and more items when
they are scrolling down the page. This article describes the design
for the list page.

* Architecture 

Conceptually, the list page is composed of three layers, illustrated
in Fig \ref{fig:list-page-arch}. The first level is the view
level. The view level defines the user interface of the page,
including progress indicators, refreshing indicator, and more; it also
directly interacts with users, producing appropriate events to be
propagated further. The third level is the data provider level. It
retrieves data from remote servers or local storage. The second level
is the mediator level. The mediator level glues the view level and the
data provider level. Moreover, it tracks the state of the view level
and the data provider level. For example, it memorizes how much data
has been loaded so far and which item is the last selected item.


#+BEGIN_SRC dot :file images/list-page-arch.png :exports results
  graph G {
    node [shape=parallelogram];

    n1 [label="View Level"];
    
    n2 [label="Mediator Level"];

    n3 [label="Data Provider Level"];

    n1 -- n2 [label="Event/Data/Widget binding"];

    n2 -- n3 [label="Invocation"];
  }

#+END_SRC

#+CAPTION: Architecure for List Page
#+NAME: fig:list-page-arch
#+ATTR_LATEX: :width 0.5\textwidth
#+RESULTS:
[[file:images/list-page-arch.png]]


The above three-layer design has the following advantages: 
  - Re-usability 
  - Extensibility 

These advantages will be elaborated in the rest of the article. Next,
we describe the interfaces among the three layers. 

** Interfaces between View/Mediator

- Event bindings
  - OnRefresh
  - OnInfinite

- Data bindings
  - asyncPush
  - syncPush
  - asyncPop
  - syncPop
  - asyncPrepend
  - syncPrepend
  - hasMoreData
- Widget bindings
  - $loader
  - $refresher 
  - $moreLoader 

** Interfaces between Mediator/Provider 

\begin{figure}
\centering
\begin{tikzpicture}
\umlinterface{IModelLike} {
  id: string || number \\ 
  attributes: object
  off?(evt: string, callback?: any) \\
  on?(evt: string, callback?: any)
}{}
\end{tikzpicture}
\caption{IModelLike}
\label{fig:imodellike}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}
\umlinterface{IDataProvider<T extends IModelLike>} {
  get models?(): Array<IModelLike> \\
  forEach(callback: (elem: IModelLike>) => any): void \\
  reset(): void \\
  getFirstPage({ data?: any }): PromiseLike<any> \\
  hasNextPage(): boolean \\
  getNextPage({ data?: any }): PromiseLike<any> \\
  off?(evt: string, callback?: any) \\
  on?(evt: string, callback?: any) 
}{}
\end{tikzpicture}
\caption{IDataProvider}
\label{fig:idataprovider}
\end{figure}

* Implementation

#+BEGIN_SRC dot :file images/list-page-work-flow.png :exports results
  digraph G {
     node [shape="box"];
     compound=true;

     x1 [label="ctor"];

     subgraph cluster2 {
         label="afterContentInit";
         n1 [label="buildMediator"];         
         n2 [label="turnOnMediator"];
     }

     subgraph cluster3 {
         label="destory";
         m1 [label="turnOffMediator"];         
         m2 [label="maybeDestroyMediator"];         
     }

     x1 -> n1 [lhead=cluster2];
     n1 -> n2;

     n2 -> m1 [lhead=cluster3, ltail=cluster2];
     m1 -> m2;
  }
#+END_SRC

#+CAPTION: Control Flow of the List Page
#+ATTR_LATEX: :width 0.4\textwidth
#+RESULTS:
[[file:images/list-page-work-flow.png]]


#+BEGIN_SRC dot :file images/list-mediator-cycle.png :exports results
  digraph G {
     node [shape="box"];
     compound=true;

     x1 [label="ctor"];

     subgraph cluster1 {
         label="active";
         x2 [label="startService"];
         x3 [label="stopService"];
     }

     x4 [label="tearDown"];

     x1 -> x2 [lhead=cluster1];
     x2 -> x3;

     x3 -> x4 [ltail=cluster1];
  }
#+END_SRC

#+CAPTION: Life Cycle of the List Meidator
#+ATTR_LATEX: :width 0.25\textwidth
#+RESULTS:
[[file:images/list-mediator-cycle.png]]


* Advance use cases

** Sliding expire cache

 

** Global provider 

#+BEGIN_SRC dot :file images/list-page-with-global-provider.png :exports results
  graph G {
    node [shape=parallelogram];

    n1 [label="View Level"];
    
    n2 [label="Mediator Level"];

    n3 [label="Network Data Provider"];

    n4 [label="View Data Provider"];

    n5 [label="Global Data Provider"];

    n1 -- n2;
    n2 -- n4;
    n2 -- n3;
    n2 -- n5;
    n3 -- n5;
    n4 -- n5; 
  }

#+END_SRC

#+CAPTION: Gloabl Provider
#+NAME: fig:list-page-with-global-provider
#+ATTR_LATEX: :width 0.9\textwidth
#+RESULTS:
[[file:images/list-page-with-global-provider.png]]

** Ngx Store


#+BEGIN_SRC dot :file images/list-page-with-ngx-store.png :exports results
  graph G {
    node [shape=parallelogram];

    n1 [label="View Level"];
    
    n2 [label="Mediator Level"];

    n3 [label="Network Data Provider"];

    n4 [label="Change detection"];

    n5 [label="Ngx Store"];

    n1 -- n2;
    n2 -- n3;
    n2 -- n4;
    n2 -- n5;
    n3 -- n5;
    n4 -- n5; 
  }

#+END_SRC

#+CAPTION: NgxStore
#+NAME: fig:list-page-with-ngx-store
#+ATTR_LATEX: :width 0.9\textwidth
#+RESULTS:
[[file:images/list-page-with-ngx-store.png]]

