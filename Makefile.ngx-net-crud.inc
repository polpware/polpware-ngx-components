NgxNetCrudBuildDist := ./dist/polpware/ngx-net-crud
NgxNetCrudDeployTarget := ./deployment/polpware-ngx-net-crud

build-ngx-net-crud:
	echo "Build ..."
	ng build @polpware/ngx-net-crud
	echo "Build done"

copy-ngx-net-crud:
	echo "Clean old files ..."
	cd $(NgxNetCrudDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxNetCrudBuildDist)/* $(NgxNetCrudDeployTarget)/
	echo "Copy files done"

push-ngx-net-crud:
	echo "Find new files ..."
	cd $(NgxNetCrudDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxNetCrudDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxNetCrudDeployTarget) && git push
	echo "Push done"

deploy-ngx-net-crud: build-ngx-net-crud copy-ngx-net-crud push-ngx-net-crud



.PHONY: build-ngx-net-crud copy-ngx-net-crud push-ngx-net-crud deploy-ngx-net-crud
