NgxSpinnerBuildDist := ./dist/polpware/ngx-spinner
NgxSpinnerDeployTarget := ./deployment/polpware-ngx-spinner

build-ngx-spinner:
	echo "Build ..."
	ng build @polpware/ngx-spinner
	echo "Build done"

copy-ngx-spinner:
	echo "Clean old files ..."
	cd $(NgxSpinnerDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxSpinnerBuildDist)/* $(NgxSpinnerDeployTarget)/
	echo "Copy files done"

push-ngx-spinner:
	echo "Find new files ..."
	cd $(NgxSpinnerDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxSpinnerDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxSpinnerDeployTarget) && git push
	echo "Push done"

deploy-ngx-spinner: build-ngx-spinner copy-ngx-spinner push-ngx-spinner



.PHONY: build-ngx-spinner copy-ngx-spinner push-ngx-spinner deploy-ngx-spinner
