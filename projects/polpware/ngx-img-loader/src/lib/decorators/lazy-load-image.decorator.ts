export function lazyLoadImageDecorator(imageBound: number = 180) {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        return class extends constructor {
            public onImageLoaded(evt) {
                const elem = evt.element.nativeElement;

                if (elem.naturalHeight < imageBound && elem.naturalWidth < imageBound) {
                    return;
                }

                if (elem.naturalHeight > elem.naturalWidth) {
                    elem.height = imageBound;
                    elem.width = elem.naturalWidth * imageBound / elem.naturalHeight;
                } else {
                    elem.width = imageBound;
                    elem.height = elem.naturalHeight * imageBound / elem.naturalWidth;
                }
            }
        };
    };
}
