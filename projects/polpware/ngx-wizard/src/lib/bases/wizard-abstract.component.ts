import { ISparseMultiArray, SparseMultiArrayImpl } from '../utils/sparse-multi-array';
import { INavElementType } from '../interfaces/nav-element-type.interface';
import { IHasWizardFeature } from '../interfaces/has-wizard-feature.interface';

export abstract class WizardAbstractComponent implements IHasWizardFeature {

    // Contract
    navigatorCfg: ISparseMultiArray<INavElementType>;
    dimIndice: number[];
    forwardDimIndice: number[];
    stepIndex: number;

    // Contract
    navigator: INavElementType;
    forwardNavigator: INavElementType;

    /*
     * Maximum dimension
     * 
     * This value is determined by the maximum steps for any choice. 
     * Think about this one as the horizontal axis. 
     */
    maxDim: number;

    // Contract 
    get hasPrevStep() {
        return this.stepIndex > 0;
    }

    get hasNextStep() {
        return this.forwardNavigator.hasNextStep;
    }

    get disableNextStep() {
        return this.forwardNavigator.hasNextGuard;
    }

    /*
     * Override the method and conduct the following 
     *
     * 
        this.navigatorCfg.setElement({
            hasPrevStep: false,
            hasNextStep: true,
            hasNextGuard: false,
            visible: true
        }, 2);

        this.stepIndex = 0;
        this.dimIndice = [0, 0, 0, 0];
        this.forwardDimIndice = [1, 0, 0, 0];

        this.navigator = this.navigatorCfg.getElement(1);
        this.forwardNavigator = this.navigatorCfg.getElement(...this.forwardDimIndice);
    */
    buildNavigatorCfg() {
        this.navigatorCfg = new SparseMultiArrayImpl(this.maxDim, () => {
            return {
                hasPrevStep: false,
                hasNextStep: false,
                hasNextGuard: false,
                visible: false
            };
        });
    }

    visible(...numbers: number[]) {

        let flag = false;
        for (let i = 0; i < this.maxDim; i++) {
            const source = numbers[i] || 0;
            const target = this.dimIndice[i];
            flag = source === target;
            if (!flag) {
                break;
            }
        }
        return flag;
    }

    protected nextStepInternal() {
        this.dimIndice = [...this.forwardDimIndice];
        this.navigator = this.navigatorCfg.getElement(...this.dimIndice);

        this.stepIndex++;
        // If the next step is not the init step, let's use it directly.
        if (this.forwardDimIndice[this.stepIndex] === 0) {
            this.forwardDimIndice[this.stepIndex] = 1;
        }

        this.forwardNavigator = this.navigatorCfg.getElement(...this.forwardDimIndice);
    }

    nextStep() {
        // Guard
        if (this.disableNextStep) {
            return;
        }

        if (this.forwardNavigator.nextStep) {
            this.forwardNavigator.nextStep();
        } else {
            this.nextStepInternal();
        }
    }

    prevStep() {
        if (this.forwardNavigator.prevStep) {
            this.forwardNavigator.prevStep();
        } else {
            this.prevStepInternal();
        }
    }

    protected prevStepInternal() {
        if (this.stepIndex > 0) {
            // Recover our options 
            this.forwardDimIndice = [...this.dimIndice];
            this.forwardNavigator = this.navigatorCfg.getElement(...this.forwardDimIndice);

            this.stepIndex--;
            this.dimIndice[this.stepIndex] = 0;

            this.navigator = this.navigatorCfg.getElement(...this.dimIndice);
        }
    }
}
