export interface ISparseMultiArray<T> {
    getElement(...numbers: number[]): T;
    setElement(val: T, ...numbers: number[]): void;
}


export class SparseMultiArrayImpl<T> {

    private readonly _impl: any;
    private readonly _maxDim: number;
    private readonly _factory: () => T;

    constructor(maxDim: number, ctor: () => T) {
        this._impl = [];
        this._maxDim = maxDim;
        this._factory = ctor;
    }

    getElement(...numbers: number[]): T {
        const len = this._maxDim > numbers.length ? this._maxDim : numbers.length;
        let data = this._impl;
        for (let i = 0; i < len; i++) {
            const dimIndex = numbers[i] || 0;
            if (data[dimIndex]) {
                data = data[dimIndex];
            } else {
                return this._factory();
            }
        }
        return data;
    }

    setElement(val: T, ...numbers: number[]) {
        const len = this._maxDim > numbers.length ? this._maxDim : numbers.length;
        let data = this._impl;
        for (let i = 0; i < len - 1; i++) {
            const dimIndex = numbers[i] || 0;
            if (!data[dimIndex]) {
                data[dimIndex] = [];
            }
            data = data[dimIndex];
        }

        // Unroll the last one
        const lastIndex = numbers[len - 1] || 0;
        data[lastIndex] = val;
    }
}
