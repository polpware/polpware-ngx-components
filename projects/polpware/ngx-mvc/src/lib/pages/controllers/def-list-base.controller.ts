
import { IFullBackboneCollectionLike, ICollectionItem } from '@polpware/fe-data';
// base
import { BackboneBackedListPage } from '../../mvc/backbone-backed-list-page';

import {
    IDefListBaseMediatorCtorOptions,
    DefListBaseMediator,
    IDefListBaseMediatorPublic
} from '../mediators/def-list-base.mediator';

export interface IDefListBaseControllerSettings {
    endpointName: string;
    tableName: string;
    cacheKey: string;
}

export abstract class DefListBaseController<T extends ICollectionItem> extends BackboneBackedListPage<T> {

    constructor(protected readonly _listSettings: IDefListBaseControllerSettings) {
        // If we navigated to this page, we will have an item available as a nav param
        super();

        this.mediatorCache = this.getGlobalCache();
    }

    public get asDefListBaseMediator(): IDefListBaseMediatorPublic {
        return this.listMediator as IDefListBaseMediatorPublic;
    }

    /**
     * Indicates whether the underlying medicator is built from the previous 
     * cache or not. 
     */
    public get fromCache() {
        // Get the fromCache value ...
        return this.listMediator ? this.asDefListBaseMediator._formCache : false;
    }

    /**
     * Indicates whether the underlying medicator is still in the init stage, 
     * I.e., the underlying mediator has not conducted any request or not. 
     */
    public get inInitState() {
        return this.listMediator ? this.asDefListBaseMediator._isInit : true;
    }

    protected getCacheKey(): string {
        return this._listSettings.cacheKey;
    }

    protected abstract getBackendService(): any;
    protected abstract getGlobalCache(): any;
    protected abstract getRelationalDB(): any;

    /**
     * Builds the underlying mediator
     * @param keyword The parameter is passed all the way from the
     * onDocumentReady method.
     */
    protected buildMediator(keyword: string): PromiseLike<void> {

        const backendService = this.getBackendService();
        const backendProvider = backendService.backendProvider;

        let globalDataProvider: IFullBackboneCollectionLike = null;

        const reDBService = this.getRelationalDB();
        const relDB = reDBService.get();

        // Build collections
        globalDataProvider = relDB.getTable(this._listSettings.tableName).dataProvider();

        // Local data provider
        // The parameter is the endpoint defined by backend.service
        const Ctor = backendProvider.getEndPoint(this._listSettings.endpointName);
        const localDataProvider = new Ctor();

        this.touchLocalDataProvider(localDataProvider);

        // Init data provider 
        localDataProvider.state.pageSize = 40;
        localDataProvider.state.keyword = keyword;

        const filterOptions = globalDataProvider ? {
            added: true,
            removed: true,
            updated: false
        } : {
                added: false,
                removed: false,
                updated: false
            };

        const ctorOptions: IDefListBaseMediatorCtorOptions = {
            globalProvider: globalDataProvider,
            filterFlags: filterOptions,
            dataProvider: localDataProvider,
            useModel: true,
            keyword: keyword,
            pageSize: 40,
            enableInfinite: true,
            enableRefresh: true
        };

        this.listMediator = this.invokeMediatorCtor(ctorOptions);
        this.listMediator.setUp();

        return Promise.resolve();
    }

    /**
     * Provides a chance to invoke a derived mediator in the derived controller.
     * @param options
     */
    protected invokeMediatorCtor(options: IDefListBaseMediatorCtorOptions) {
        return new DefListBaseMediator(options);
    }

    /**
     * Provides a chance to update the freshly generated data provider.
     * E.g., we can use this method to update the endpoint url. 
     */
    protected touchLocalDataProvider(dataProvider: any) {
    }

}
