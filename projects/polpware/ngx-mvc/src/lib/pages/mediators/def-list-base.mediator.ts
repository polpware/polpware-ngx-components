import { IRxjsPoweredDirContentMediatorDev, IWritableListMediatorCtorOptions, IWritableListMediatorPublic, RxjsPoweredWritableListMediator } from '@polpware/fe-mvc';

export interface IDefListBaseMediatorCtorOptions extends IWritableListMediatorCtorOptions {
    keyword?: string;
    pageSize?: number;
}

/** Specifies the internal interface for accessing the properties 
 * of the internal implementation of a mediator.
 * 
 * This interface is supposed used only in the traditional way of implementing a 
 * a class, e.g., the way of xx.extend({}). 
 * 
 * Using this interface helps the editor to figure out the correct properties 
 * when we implement the mediator.
 */
export interface IDefListBaseMediatorDev extends IRxjsPoweredDirContentMediatorDev {
    _filter: string;
    _keyword: string;
    _pageSize: number;
    _fromCache: boolean;

    reComputeDataParams();
}

/** Specifies the interface that we can use in the controller 
 * which uses the mediator.
 *  
 * This interface and the above interface describes the same object in 
 * two distinct perspectives. The above one defines the interface from the 
 * perspective of implmenting a meditator. This one defines the interface 
 * from the perspective of a client.
 * 
 * Using this interface helps the editor to figure out the correct methods 
 * we may use in the controller and its sub-classes.
 */
export interface IDefListBaseMediatorPublic extends IWritableListMediatorPublic {

    // filter is not exposed yet, because we do not know how to use it in the controller yet.

    // We only manipulate pageSize internally, and then
    // it is not necessary to expose it to the controller.

    keyword(value?: string): string;

    // Read the value of formCache
    _formCache: boolean;
    _isInit: boolean;
}

export const DefListBaseMediator = RxjsPoweredWritableListMediator.extend({

    /* Properties */
    Properties: 'filter,keyword,pageSize',

    /**
     * Override
     * 
     * @param settings
     */
    init: function(settings: IDefListBaseMediatorCtorOptions) {
        const self: IDefListBaseMediatorDev = this;
        self._super(settings);

        self._filter = '';
        // Init 
        self._keyword = settings.keyword || '';
        self._pageSize = settings.pageSize || 40;
        self._fromCache = false;
    },

    /**
     * Override 
     * Render data in 
     * @param asyncLoaded
     */
    renderData: function(asyncLoaded: boolean) {
        const self: IDefListBaseMediatorDev = this;
        self._super(asyncLoaded);
    },

    /**
     * Override 
     * so that we can reload data even in the case of cache
     * @param {} fromCache
     */
    startService: function(viewInstance, fromCache: boolean) {
        const self: IDefListBaseMediatorDev = this;
        self.attachView(viewInstance);
        if (fromCache === true) {
            self._fromCache = true;
            self.renderData(true);
        } else {
            self._fromCache = false;
            // Enforce that keyword is ''
            self.startServiceImpl();
        }
    },

    /**
     * Override
     */
    reComputeDataParams: function() {
        const self: IDefListBaseMediatorDev = this;
        // target
        const state = self._dataProvider.state;
        state.offset = 0;
        state.keyword = self._keyword || '';
    },

    /**
     * Override
     */
    loadInitData: function() {
        const self: IDefListBaseMediatorDev = this;
        self.reComputeDataParams();
        return self._super();
    }

});
