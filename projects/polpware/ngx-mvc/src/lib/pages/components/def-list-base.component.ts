import { ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ICollectionItem } from '@polpware/fe-data';
import { INgxNoty } from '@polpware/ngx-noty';
import { Subscription } from 'rxjs';
import { DefListBaseController, IDefListBaseControllerSettings } from '../controllers/def-list-base.controller';

export interface IDefListBaseComponentSettings extends IDefListBaseControllerSettings {
    spinnerName: string;
}

export interface ISpinnerLike {
    show(...args: any[]);
    hide(...args: any[]);
    startToListenSpinner(...args: any[]);
    stopListener(...args: any[]);
}

export abstract class DefListBaseComponent<T extends ICollectionItem> extends DefListBaseController<T> {

    @Input() bottomOffset = 0;
    @Input() minHeight = 0;
    @Input() fixedHeight = 0;
    @Input() maxHeight = 0;
    @Input() topOffset = 0;
    @Input() containerClass = '';
    @Input() initHighlightId: string = '';

    @Output() onSelect = new EventEmitter();

    @ViewChild('searchControlElem')
    searchControlElem: ElementRef;

    ////////////////////////////////////////////////////////////////////////////////
    // Defines the states for the search state machine
    //   waitForInput --> typeKeywordState
    ////////////////////////////////////////////////////////////////////////////////
    // 
    public searchEnabled: boolean;
    // 
    // This flag Decides if we can display a control
    // which further decides the visibility of the search input control.
    //
    // This flag is used when we have another level of
    // controlling whether the search input should be visible or not.
    // E.g., When the space is limited, we may display a control flag
    // to turn on the visiblity of the real search input, and
    // by default only shows the control flag.
    // 
    public waitForInputState: boolean;
    //
    // This flag decides if the search input control should be visible
    // or not. 
    // 
    public typeKeywordState: boolean;
    //
    // This property tracks the current effective keyword. 
    // 
    public keywordInEffect: string;
    //
    // This flag decides if any keyword is in effctive.
    //
    // It is used when generating the state of the search result.
    // 
    public keywordInEffectState: boolean;
    // Search control input
    public searchControl: FormControl;

    //
    // This property tracks if there is any keyword 
    // which may be applied in the future.
    // E.g., though there is a keyword in effect,
    // a user may enter new keyword in the search input control
    // and the new value is not equal to the current effective
    // keyword. In this case, anyFutureKeyword tells the new value. 
    public anyFutureKeyword: string;

    //
    // Tracks the currently selected item.
    // We decide not to change the value of the selected item.
    // Instead, each controller may have its own selected item.
    // Doing so, there is no interference among different controllers,
    // even though they share the same underlying data. 
    public selected: T;

    private _searchKeywordSubr: Subscription;

    constructor(listSettings: IDefListBaseComponentSettings,
        protected readonly _spinner: ISpinnerLike,
        protected readonly _toastr: INgxNoty) {
        super(listSettings);
        // By default, search is enabled
        this.searchEnabled = true;
        this.searchControl = new FormControl('');
    }

    // Compute the total number of records from the underlying mediator
    // and further the data provider of the mediator.
    get totalCount() {
        return this.asDefListBaseMediator.dataProvider().state.totalRecords;
    }

    // As above, compute the loaded number of records so far.
    get offset() {
        return this.asDefListBaseMediator.dataProvider().state.totalRecords;
    }

    get spinnerName() {
        return (this._listSettings as IDefListBaseComponentSettings).spinnerName;
    }

    ngOnInit(): void {
        this._spinner.startToListenSpinner(this.spinnerName);

        this.onDocumentReady();
        this.startObserveSearchKeyword();
    }

    ngOnDestroy() {
        this._spinner.stopListener(this.spinnerName);

        this.onDocumentDestroy();
        this.stopObserveSearchKeyword();
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Overrides to tweak the behaviors of the loading/unloading logic
    ////////////////////////////////////////////////////////////////////////////////


    /**
     * Following building a mediator or retrieving a mediator from cache, 
     * this method turns on the mediator to trigger network request. 
     * 
     * @param fromCache
     * @param keyword The parameters from the second one are passed all the way from the 
     * onDocumentReady method.
     */
    protected turnOnMediator(fromCache: boolean, keyword: string) {
        super.turnOnMediator(fromCache, keyword);

        // TODO: Check if we need the following logic?

        // if (this.searchEnabled) {
        //     // Synchronizing the UI and the internal state
        //     const keyword = this.asDefListBaseMediator.keyword();
        //     if (keyword) {
        //         keyword = keyword.toLowerCase();
        //         this.searchControl.setValue(keyword, {
        //             emitEvent: false
        //         });
        //     }
        // }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Indicators
    ////////////////////////////////////////////////////////////////////////////////

    // Override
    public showLoadingIndicator() {
        this._spinner.show('Loading ...', this.spinnerName);
    }

    public hideLoadingIndicator() {
        this._spinner.hide(this.spinnerName);
    }

    // Override
    public showMoreLoading(): void {
        this._spinner.show('Loading ...', this.spinnerName);
    }

    // Override
    public hideMoreLoading(): void {
        this._spinner.hide(this.spinnerName);
    }

    // Override
    public showRefreshingIndicator(): void {
        this._spinner.show('Loading ...', this.spinnerName);
    }

    // Override
    public hideRefreshingIndicator(): void {
        this._spinner.hide(this.spinnerName)
        // Release a message 
        this._toastr.success(`List was just refreshed.`,
            'Success', {
            closeButton: true
        });

    }

    ////////////////////////////////////////////////////////////////////////////////
    // Search state machine
    ////////////////////////////////////////////////////////////////////////////////

    // Start to listen for search keyword change
    protected startObserveSearchKeyword() {
        this._searchKeywordSubr = this.searchControl.valueChanges.subscribe(a => {
            a = (a || '').toLowerCase();
            if (a && a !== this.keywordInEffect) {
                this.anyFutureKeyword = a;
            } else {
                this.anyFutureKeyword = '';
            }
        });
    }

    protected stopObserveSearchKeyword() {
        this._searchKeywordSubr && this._searchKeywordSubr.unsubscribe();
    }

    // Recomputes the search state
    //
    // 
    protected computeSearchState() {
        this.anyFutureKeyword = '';
        this.keywordInEffectState = false;
        this.typeKeywordState = false;
        this.waitForInputState = false;
        let keyword = this.asDefListBaseMediator.keyword();

        if (keyword) {
            keyword = keyword.toLowerCase();
            this.keywordInEffect = keyword;
            this.keywordInEffectState = true;

            // Make sure that the search input has the latest value
            let rhs = this.searchControl.value || '';
            rhs = rhs.toLowerCase();
            if (rhs !== keyword) {
                this.searchControl.setValue(keyword, {
                    emitEvent: false
                });
            }

        } else {
            this.waitForInputState = true;

            // Make sure that the search input has the latest value
            let rhs = this.searchControl.value || '';
            rhs = rhs.toLowerCase();
            if (rhs) {
                this.searchControl.setValue('', {
                    emitEvent: false
                });
            }
        }

    }

    // Swtiches to the state for providing
    // the search input control for end users.
    // 
    public startToTypeKeyword() {
        this.anyFutureKeyword = '';
        this.waitForInputState = false;
        this.keywordInEffectState = false;
        this.typeKeywordState = true;

        // Schedule focus behavior in next round of UI updating,
        // in order that the above settings are already in effect.
        setTimeout(() => {
            // TODO: Fix this
            // this.focusFolderSearchInput();
        });
    }

    // Cancel typed keyword and
    // reset to whatever the previous state
    //
    // This operation does not cause new network request.
    public cancelTypedKeyword() {
        this.computeSearchState();

        // Auto focus the search input
        this.searchControlElem.nativeElement.focus();
    }

    // Clear up keyword
    //
    // This operation causes new network request.
    public clearKeywordInEffect() {
        this.asDefListBaseMediator.keyword('');
        this.asDefListBaseMediator.refresh(true);

        // Auto focus the search input
        this.searchControlElem.nativeElement.focus();
    }

    // Starts a new round of search
    //
    // This operation causes new network request.
    public kickOffSearch() {
        const k = this.searchControl.value;
        // TODO: Normalize into lowercase ?

        const currentKeyword = this.asDefListBaseMediator.keyword;
        if (k === currentKeyword) {
            // Nothing to do;
            this.computeSearchState();
            return;
        }

        // Otherwise, move forward to search 
        this.asDefListBaseMediator.keyword(k);
        this.asDefListBaseMediator.refresh(true);
    }


    // Override
    //
    // The extra operation allows for synchronizing the internal state
    // with the user interface.
    public onItemsReady(): void {
        super.onItemsReady();

        this.computeSearchState();

        if (this.initHighlightId) {
            this.highlight(this.initHighlightId);
        }
    }

    /**
      * Sends a notification back to its parent or client.
      * @param item A data entity.
      */
    selectItem(item: T) {
        this.initHighlightId = null;
        this.selected = item;
        this.onSelect.emit(item);
    }

    /**
     * Allows the client to highlight an item by Id.
     * @param id
     */
    highlight(id: string) {
        const item = this.items.find(a => a.id == id);
        if (item && this.selected !== item) {
            this.selected = item;
        }
    }

}
