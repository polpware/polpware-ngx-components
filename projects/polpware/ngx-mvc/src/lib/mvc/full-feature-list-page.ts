import { IViewInstance } from '@polpware/fe-mvc';
import { IListMediatorPublic } from '@polpware/fe-mvc';
// base
import {
    PlatformObliviousListPage
} from './platform-oblivious-list-page';

import {
    adaptAngularToController
} from './adaptors/angular-to-controller-adaptor';

export interface IPageLifeCycle {
    onDocumentReady(...args: Array<any>): void;
    onDocumentDestroy(...args: Array<any>): void;
}

export abstract class FullFeatureListPage
    extends PlatformObliviousListPage implements IPageLifeCycle {

    onDocumentReady(...args: Array<any>) {
        // Cache will be provided in its derived class
        this.ensureDataProvider(...args);
    }

    onDocumentDestroy(...args: Array<any>) {

        // Cache will be provided in its derived class

        this.turnOffMediator();
        this.afterMediatorOff();
    }

    protected abstract ensureDataProvider(...args: Array<any>): void;
    protected abstract afterMediatorOn(): void;
    protected abstract afterMediatorOff(): void;

    protected abstract readMediatorFromCache(key: string): IListMediatorPublic;
    protected abstract writeMediatorIntoCache(key: string, value: IListMediatorPublic): void;
    protected abstract addOnCacheExpireHandler(key: string): void;
    protected abstract removeOnCacheExpireHandler(key: string): void;

    // May be not needed. 
    protected onDataProviderReady(dataProvider: any): void {
        this.buildMediator(dataProvider).then(() => {
            this.turnOnMediator(false);
            this.afterMediatorOn();
        });
    }

    // Override
    protected buildViewInstance(): IViewInstance {
        return adaptAngularToController({
            $scope: this
        });
    }

    public doRefresh() {
        // Trigger refresh
        if (this.callbacks.onRefresh) {
            this.callbacks.onRefresh();
        }
    }

    public doInfinite() {
        // Trigger loading more
        if (this.callbacks.onInfinite) {
            this.callbacks.onInfinite();
        }
    }

    public showLoadingIndicator(...args: any[]) { }

    public hideLoadingIndicator(...args: any[]) { }

    public setLoadingIndicatorDelay(seconds: number) { }

    public showMoreLoading(...args: any[]) { }

    public hideMoreLoading(...args: any[]) { }

    public showRefreshingIndicator(...args: any[]) { }

    public hideRefreshingIndicator(...args: any[]) { }

}

