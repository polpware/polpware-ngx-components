import { Subscription } from 'rxjs';

import { IViewInstance } from '@polpware/fe-mvc';

import { pushArray } from '@polpware/fe-utilities';

import {
    WritableListMediator,
    IWritableListMediatorCtorOptions,
    IWritableListMediatorPublic
} from '@polpware/fe-mvc';

import {
    RxjsPoweredWritableListMediator
} from '@polpware/fe-mvc';

import {
    ICollectionItem
} from '@polpware/fe-data';

import {
    ISlidingExpireCache
} from '@polpware/fe-data';

// base
import { FullFeatureListPage } from './full-feature-list-page';

// Note that we use ICollectionItem rather than IModelLike,
// because we assume the least requirement for the input type.
// Precisely, the only requirement is that the collection item has an
// id field. 

export abstract class BackboneBackedListPage<T extends ICollectionItem>
    extends FullFeatureListPage {

    protected defaultLivePeriod = 60 * 5;

    protected mediatorCache: ISlidingExpireCache<IWritableListMediatorPublic>;
    private _onCacheExpireCallback: any;

    public items: T[];

    constructor() {
        super();

        this.items = [];
        this._onCacheExpireCallback = null;
    }

    // We use a functon to compute cacheKey, so that we can
    // compute the cache key with more inputs. 
    protected abstract getCacheKey(...args: any[]): string;

    protected get asWritableListMediator(): IWritableListMediatorPublic {
        const m = this.listMediator;
        return m as IWritableListMediatorPublic;
    }

    // Default implementation for using backbone
    protected useMediatorWithOnlyLocalDataProvider(localDataProvider: any, localOptions?: object) {

        const ctorOptions: IWritableListMediatorCtorOptions = {
            dataProvider: localDataProvider,
            useModel: true,
            enableInfinite: true,
            enableRefresh: true
        };

        const s = new WritableListMediator(ctorOptions);
        this.listMediator = s;
        this.listMediator.setUp();
    }

    protected useMediatorWithGlobalDataProvider(localDataProvider: any, globalDataProvider: any,
        localOptions?: object, globalOptions?: object) {

        const mediator = new RxjsPoweredWritableListMediator({
            globalProvider: globalDataProvider,
            filterFlags: {
                added: true,
                removed: true,
                updated: false
            },
            dataProvider: localDataProvider,
            useModel: true
        });

        this.listMediator = mediator;
        this.listMediator.setUp();
    }

    // Invoked after the new mediator is constructure 
    protected postUseFreshMediator(...args: any[]) {
        this.turnOnMediator(false, ...args);
        this.afterMediatorOn();
    }

    // Invoked after the cached mediator is used 
    protected postUseCachedMediator(...args: any[]) {
        this.turnOnMediator(true, ...args);
        this.afterMediatorOn();
    }

    // Override to support cache
    protected ensureDataProvider(...args: any[]) {
        if (this.mediatorCache) {

            const cacheKey = this.getCacheKey(...args);
            let inCache = false;

            const mediator = this.readMediatorFromCache(cacheKey);

            if (!mediator) { // Not in cache

                this.buildMediator(...args).then(() => {
                    // set up in the cache
                    this.writeMediatorIntoCache(cacheKey, this.asWritableListMediator);

                    // case 1:
                    this.postUseFreshMediator(true, ...args);
                });

            } else { // In cache

                inCache = true;
                this.listMediator = mediator;

                // Case 2:
                this.postUseCachedMediator(...args);
            }

        } else {

            this.buildMediator(...args).then(() => {

                // Case 3: 
                this.postUseFreshMediator(false, ...args);
            });
        }
    }

    // Override
    protected afterMediatorOn() {
        if (this.mediatorCache) {
            // In this case, we do not Provide any inputs 
            const cacheKey = this.getCacheKey();
            this.addOnCacheExpireHandler(cacheKey);
        }
    }

    // Override
    protected afterMediatorOff() {
        if (this.mediatorCache) {
            // In this case, we do not Provide any inputs
            const cacheKey = this.getCacheKey();
            this.removeOnCacheExpireHandler(cacheKey);
        }
    }


    // Default implementation
    public onNewItemsReady(items: Array<any>): Array<any> {
        pushArray(this.items, items);
        return items;
    }

    // Default implementation.
    onItemsReady() {
        const viewData = this.asWritableListMediator.viewLevelData();
        // Get the data from the view level data 
        this.items = viewData.models.slice(0);
    }

    // Note that it is up to the caller to decide how to use the
    // cached value; we need to precisely tell where there is a value in the cache
    // for the corresponding key
    protected readMediatorFromCache(key: string): IWritableListMediatorPublic {
        return this.mediatorCache.get(key, this.defaultLivePeriod);
    }

    protected writeMediatorIntoCache(key: string, mediator: IWritableListMediatorPublic): void {
        this.mediatorCache.set(key, mediator, this.defaultLivePeriod, (evt) => {
            mediator.tearDown();
            return evt;
        });
    }

    protected addOnCacheExpireHandler(key: string): void {
        this._onCacheExpireCallback = function(evt) {
            evt.preventDefault();
            return evt;
        };

        this.mediatorCache.addOnExpireHandler(key, this._onCacheExpireCallback);
    }

    protected removeOnCacheExpireHandler(key: string): void {
        this.mediatorCache.rmOnExpireHandler(key, this._onCacheExpireCallback);
        this._onCacheExpireCallback = null;
    }

}

