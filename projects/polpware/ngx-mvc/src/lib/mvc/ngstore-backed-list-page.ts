import { Subscription } from 'rxjs';

import {
    IListMediatorCtorOptions
} from '@polpware/fe-mvc';

import {
    NgStoreListMediator,
    INgStoreListMediatorPublic
} from '@polpware/fe-mvc';

import {
    ICollectionItem
} from '@polpware/fe-data';

import {
    CollectionStore
} from '@polpware/fe-data';


import {
    ISlidingExpireCache
} from '@polpware/fe-data';

// base
import { FullFeatureListPage } from './full-feature-list-page';

// Note that in the class, please avoid to depend on onNewItemsReady,
// as it is NOT in the update flow.

export abstract class NgStoreBackedListPage<T extends ICollectionItem>
    extends FullFeatureListPage {

    protected defaultLivePeriod = 60 * 5;

    // More configuration for constructing dataprovider
    protected mediatorCtorOptions: IListMediatorCtorOptions;

    protected mediatorCache: ISlidingExpireCache<INgStoreListMediatorPublic>;
    private _onCacheExpireCallback: any;

    private _storeSubscription: Subscription;
    public items: T[];

    constructor() {
        super();

        this.mediatorCtorOptions = {
            useModel: true,
            enableInfinite: true,
            enableRefresh: true
        };
        this.items = [];
        this._onCacheExpireCallback = null;
    }

    // Override
    protected turnOnMediator(fromCache: boolean) {
        super.turnOnMediator(fromCache);

        const store = this.asNgStoreListMeidator.getNgStore();
        this._storeSubscription = store.getState().subscribe((data) => {
            const w = data.items as T[];
            this.items = w;
            // Note that we must call onItemsReady ... 
            this.onItemsReady();
        });
    }

    // Override
    protected turnOffMediator() {
        this._storeSubscription.unsubscribe();
        super.turnOffMediator();
    }

    // Override
    protected buildMediator(dataProvider: any): PromiseLike<void> {

        const ctorOptions: IListMediatorCtorOptions = {
            ...this.mediatorCtorOptions,
            dataProvider: dataProvider
        };

        const s = new CollectionStore<T>();

        const m: INgStoreListMediatorPublic = new NgStoreListMediator(ctorOptions);
        m.setNgStore(s);

        this.listMediator = m;
        this.listMediator.setUp();

        return Promise.resolve();
    }

    protected get asNgStoreListMeidator(): INgStoreListMediatorPublic {
        const m = this.listMediator;
        return m as INgStoreListMediatorPublic;
    }

    protected readMediatorFromCache(key: string): INgStoreListMediatorPublic {
        return this.mediatorCache.get(key, this.defaultLivePeriod);
    }

    protected writeMediatorIntoCache(key: string, value: INgStoreListMediatorPublic): void {
        this.mediatorCache.set(key, value, this.defaultLivePeriod, (evt) => {
            value.tearDown();
            return evt;
        });
    }

    protected addOnCacheExpireHandler(key: string): void {
        this._onCacheExpireCallback = function(evt) {
            evt.preventDefault();
            return evt;
        };

        this.mediatorCache.addOnExpireHandler(key, this._onCacheExpireCallback);
    }

    protected removeOnCacheExpireHandler(key: string): void {
        this.mediatorCache.rmOnExpireHandler(key, this._onCacheExpireCallback);
        this._onCacheExpireCallback = null;
    }

    // Default implementation.
    // Override
    // Note that in the derived class we do NOT depend on it.
    onNewItemsReady(items: T[]) {
        return items;
    }

}

