/**
 * @fileOverview
 * This abstract class defines a base class for implementing
 * a page with such features as refreshing, loading more, and
 * listening to changes from a global database and inserting
 * or deleting elements accordingly.
 *
 * This class does not depend on any features that a specific
 * platform may provide, such as ionViewDidload and ...unload.
 *
 * @name PlatformAgosticFullFeatureListPage.ts
 * @author Xiaolong Tang <xxlongtang@gmail.com>
 * @license Copyright @me
 */

// By default, we do not listen to any change ..

import * as hInterface from '@polpware/fe-dependencies';

import { IViewInstance } from '@polpware/fe-mvc';

import { IListMediatorPublic } from '@polpware/fe-mvc';
import { IMediatorCompatiblePage } from './mediator-compatible-page.interface';

import {
    ILoadingIndicator,
    IRefreshingIndicator,
    ILoadingMoreIndicator
} from '../interfaces/indicators.interface';

const _ = hInterface.underscore;

export abstract class PlatformObliviousListPage
    implements IMediatorCompatiblePage,
    ILoadingIndicator,
    IRefreshingIndicator,
    ILoadingMoreIndicator {

    public moreDataCanBeLoaded: boolean;
    public callbacks: {
        onRefresh: any,
        onInfinite: any
    };

    protected listMediator: IListMediatorPublic;

    constructor() {
        this.moreDataCanBeLoaded = false;
        this.callbacks = {
            onRefresh: null,
            onInfinite: null
        };
    }

    protected abstract buildViewInstance(): IViewInstance;

    protected abstract buildMediator(...args: any[]): PromiseLike<void>;

    protected turnOnMediator(fromCache: boolean, ...rest: any[]) {
        const viewInstance = this.buildViewInstance();
        this.listMediator.startService(viewInstance, fromCache);
    }

    protected turnOffMediator() {
        this.listMediator.stopService();
    }

    public abstract showLoadingIndicator(...args: Array<any>): void;
    public abstract hideLoadingIndicator(...args: Array<any>): void;
    public abstract setLoadingIndicatorDelay(seconds: number): void;

    public abstract showMoreLoading(...args: Array<any>): void;
    public abstract hideMoreLoading(...args: Array<any>): void;

    public abstract showRefreshingIndicator(...args: Array<any>): void;
    public abstract hideRefreshingIndicator(...args: Array<any>): void;

    // Will be invoked from the adaptor and therefore
    // must be public
    public abstract onNewItemsReady(items: Array<any>): Array<any>;

    public abstract onItemsReady(): void;
}

