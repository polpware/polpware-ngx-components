import * as dependencies from '@polpware/fe-dependencies';
import { pushArray } from '@polpware/fe-utilities';

import { IViewInstance } from '@polpware/fe-mvc';

const _ = dependencies.underscore;
const noop = _.noop;

export function adaptAngularToController(context): IViewInstance {
    return {
        $data: {
            init: function() {
                context.$scope.moreDataCanBeLoaded = false;
            },
            setRefreshCallback: function(callback) {
                context.$scope.callbacks.onRefresh = callback;
            },
            setInfiniteCallback: function(callback) {
                context.$scope.callbacks.onInfinite = callback;
            },
            clean: function() {
                context.$scope.onItemsReady();
            },
            asyncPush: function(items) {
                context.$scope.onNewItemsReady(items);
                context.$scope.onItemsReady();
            },
            syncPush: function(items) {
                context.$scope.onNewItemsReady(items);
                context.$scope.onItemsReady();
            },

            asyncPop: function(items) {
                context.$scope.onItemsReady();
            },
            syncPop: function(items) {
                context.$scope.onItemsReady();
            },
            asyncPrepend: function(items) {
                context.$scope.onNewItemsReady(items);
                context.$scope.onItemsReady();
            },
            syncPrepend: function(items) {
                context.$scope.onNewItemsReady(items);
                context.$scope.onItemsReady();
            },
            asyncRefresh: noop,
            syncRefresh: noop,
            hasMoreData: function(flag) {
                context.$scope.moreDataCanBeLoaded = flag;
            },
            getItems: function() {
            },
            setupSearch: function(criteria, callback) {
                context.$scope.searchCriteria = criteria;
                context.$scope.doSearch = callback;
            },
            updateSearchCriteria: function(criteria) {
                context.$scope.searchCriteria = criteria;
            },
            getAncestor: function() {
                return context.$scope.ancestor;
            }
        },
        $loader: {
            show: function() {
                context.$scope.showLoadingIndicator();
            },
            hide: function() {
                context.$scope.hideLoadingIndicator();
            }
        },
        $refresher: {
            show: function() {
                context.$scope.showRefreshingIndicator();
            },
            hide: function() {
                context.$scope.hideRefreshingIndicator();
            }
        },
        $moreLoader: {
            show: function() {
                context.$scope.showMoreLoading();
            },
            hide: function() {
                context.$scope.hideMoreLoading();
            }
        },
        $router: {
            go: function(url, data) {
                context.$state.go(url, data);
            }
        },
        $render: {
            ready: function(callback) {
                context.$scope.callbacks.onViewDidLoad = callback;
            },
            destroy: function(callback) {
                context.$scope.callbacks.onViewWillUnload = callback;
            },
            asyncDigest: noop
        },
        $navBar: {
            /**
             * Get current state
             * @returns {}
             */
            getState: noop,
            /**
             * Set state
             * @param {Boolean} s
             */
            setState: noop
        },
        $modal: {
            setData: function(key, data) {
                context.$scope[key] = data;
            },
            getData: function(key) {
                return context.$scope[key];
            },
            build: noop
        },
        $popover: {
            setData: function(key, data) {
                context.$scope[key] = data;
            },
            getData: function(key) {
                return context.$scope[key];
            },
            build: noop,
            onHidden: noop
        },
        $popup: {
            setData: function(data) {
                context.$scope.popupInput = _.extend({
                    confirmed: false
                }, data);
            },
            getData: function() {
                return context.$scope.popupInput;
            },
            build: noop,
            confirm: noop,
            prompt: noop,
            alert: noop
        },
        $progressBar: {
            create: noop,
            reset: function() {
                context.$scope.progressBar = 0;
            },
            createInfinite: noop,
            onProgress: function(percentage) {
                context.$scope.progressBar = percentage;
            },
            destroy: noop,
            destroyInfinite: noop,
            showAbort: noop
        },
        $alertify: context.alertify,
        $history: {
            goBack: function() {
                context.$ionicHistory.goBack();
            }
        }
    };
}

