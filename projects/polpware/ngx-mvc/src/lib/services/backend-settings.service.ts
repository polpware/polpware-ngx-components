import { InjectionToken } from '@angular/core';

export class BackendSettings {
    urlBase: string;
}

export const BACKEND_SETTINGS = new InjectionToken<BackendSettings>('Backend Settings');
