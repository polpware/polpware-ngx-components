import { INgxLogger } from '../interfaces/ngx-logger.interface';
import { ILoggerProvider } from '../interfaces/logger-provider.interface';
import { NgxLoggerImpl } from './ngx-logger.impl';

/**
 * We on purpose do not make this class to have a root provider. 
 * So that the application feels free to set up it. 
 */
export class LoggerProviderImpl implements ILoggerProvider {
    defaultImpl = new NgxLoggerImpl();

    logger(k: string) {
        return this.defaultImpl;
    }
}
