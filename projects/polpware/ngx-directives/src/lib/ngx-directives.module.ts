import { NgModule } from '@angular/core';
import { RestWindowHeightDirective } from './rest-window-height/rest-window-height.directive';

@NgModule({
    declarations: [
        RestWindowHeightDirective],
    imports: [
    ],
    exports: [RestWindowHeightDirective]
})
export class PolpNgxDirectivesModule { }
