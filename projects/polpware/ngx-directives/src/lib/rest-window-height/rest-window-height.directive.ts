import { Directive, ElementRef, AfterViewInit, Input, HostListener } from '@angular/core';

function findAncestorByClass(el, cls: string) {
    while (el.parentElement) {
        el = el.parentElement;
        if (el.classList.contains(cls)) {
            break;
        }
    }
    return el;
}

/**
 * Defines a directive for setting the height of the element in question according 
 * to a formula.
 * 
 */
@Directive({
    selector: '[polpRestWindowHeight]'
})
export class RestWindowHeightDirective implements AfterViewInit {

    @Input() bottomOffset = 0;
    @Input() minHeight = 0;
    @Input() fixedHeight = 0;
    @Input() maxHeight = 0;
    @Input() topOffset = 0;
    @Input() containerClass = '';

    constructor(private el: ElementRef) { }

    ngAfterViewInit() {
        setTimeout(() => {
            this.computeHeight();
        });
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        setTimeout(() => {
            this.computeHeight();
        });
    }

    private computeHeight() {
        const nativeElement = this.el.nativeElement;
        // Case 1: fixed Height 
        if (this.fixedHeight) {
            this.el.nativeElement.style.height = this.fixedHeight + 'px';
            return;
        }

        let bodyRect: DOMRect;
        if (this.containerClass) {
            const p = findAncestorByClass(nativeElement, this.containerClass);
            bodyRect = p.getBoundingClientRect();
        } else {
            bodyRect = document.body.getBoundingClientRect();
        }

        const elemRect = nativeElement.getBoundingClientRect();

        const offset = elemRect.top - bodyRect.top;

        const screenHeight = window.innerHeight;

        let height = screenHeight - offset - this.bottomOffset - this.topOffset;

        height = (height > this.minHeight) ? height : this.minHeight;

        if (this.maxHeight) {
            height = (height > this.maxHeight) ? this.maxHeight : height;
        }

        this.el.nativeElement.style.height = height + 'px';
    }


}
