import { Injector } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';

import { liftIntoReject } from '@polpware/fe-utilities';

import { toPromise } from '@polpware/ngx-rxjs';

import {
    GlobalEventsService
} from '@polpware/ngx-events';

import {
    ISpinnerService,
    NullSpinner
} from '@polpware/ngx-spinner';

import {
    IBaseEntity
} from '@polpware/ngx-model';

export {
    IBaseEntity
} from '@polpware/ngx-model';

export abstract class ObservableCurdService<T extends IBaseEntity> {

    protected subject: BehaviorSubject<Array<T>>;

    protected http: HttpClient;
    protected eventsService: GlobalEventsService;
    protected spinner: ISpinnerService;

    protected withCredentialOnRequest: boolean;

    constructor(injector: Injector) {

        this.withCredentialOnRequest = true;

        this.subject = new BehaviorSubject([]);

        this.http = injector.get(HttpClient);
        this.eventsService = injector.get(GlobalEventsService);

        this.spinner = new NullSpinner();
    }

    // *************************************
    // abstract methods
    // *************************************
    protected abstract listUrl(...args: any[]): string;
    protected abstract deleteUrl(...args: any[]): string;
    protected abstract createUrl(...args: any[]): string;
    protected abstract updateUrl(...args: any[]): string;

    protected abstract getListGuard(): boolean;
    protected abstract deleteByIdGuard(id: string): boolean;

    protected abstract notifyList(data: Array<T>): void;
    protected abstract notifyDelete(id: string): void;
    protected abstract notifyCreate(record: T);
    protected abstract notifyUpdate(record: T);

    abstract getById(id: string): T;
    abstract getByIdAsync(id: string, mySpinner: ISpinnerService): PromiseLike<T>;

    protected handleError(error: HttpErrorResponse) {
        this.eventsService.broadcast('http-error', [error]);
    }

    public onDataChange(): Observable<Array<T>> {
        return this.subject.asObservable();
    }

    // Default methods
    protected parseCreateResponse(record: T, data): T {
        const id = data;
        record.id = id;
        return record;
    }

    protected parseUpdateResponse(record: T, data): T {
        return record;
    }

    protected parseListResponse(data): T[] {
        return data;
    }

    // Returns a list of entities
    protected listRequest(options: { [key: string]: any }): Observable<T[]> {
        let httpParams = new HttpParams();
        for (const k in options) {
            if (options.hasOwnProperty(k)) {
                httpParams = httpParams.set(k, options[k]);
            }
        }

        return this.http.get<T[]>(this.listUrl(), {
            withCredentials: this.withCredentialOnRequest,
            params: httpParams
        });
    }

    getListAsync(options: { [key: string]: any }, mySpinner: ISpinnerService = null): PromiseLike<any> {
        // In most cases, we do not need to send out a request
        // if we already have some data.
        if (this.getListGuard()) {
            return liftIntoReject('not allowed');
        }

        const spinner = mySpinner || this.spinner;

        spinner.show();
        return toPromise(this.listRequest(options))
            .then((data) => {
                spinner.hide();

                const newData = this.parseListResponse(data);
                this.notifyList(newData);

                return newData;
            }, (error) => {
                spinner.hide();

                this.handleError(error);

                return error;
            });
    }

    // Use post instead of delete method to implement delelete ??
    protected deleteByIdRequest(id: string): Observable<{}> {
        return this.http.delete(this.deleteUrl(id), {
            withCredentials: this.withCredentialOnRequest
        });
    }

    deleteByIdAsync(id: string, mySpinner: ISpinnerService = null): PromiseLike<any> {

        if (this.deleteByIdGuard(id)) {
            return liftIntoReject('not allowed');
        }

        const spinner = mySpinner || this.spinner;
        spinner.show();
        return toPromise(this.deleteByIdRequest(id))
            .then((x) => {
                spinner.hide();

                this.notifyDelete(id);
                return id;
            }, (error) => {
                spinner.hide();

                this.handleError(error);

                return error;
            });
    }


    protected adaptorCreateInput(record: T): Object {
        return record;
    }

    protected createRequest(record: T): Observable<T> {
        const body = {};
        const tyRecord = this.adaptorCreateInput(record);
        for (const prop in tyRecord) {
            if (tyRecord.hasOwnProperty(prop)) {
                body[prop] = tyRecord[prop];
            }
        }
        return this.http.post<T>(this.createUrl(), body, {
            withCredentials: this.withCredentialOnRequest
        });
    }

    createAsync(record: T, mySpinner: ISpinnerService = null): PromiseLike<any> {

        const spinner = mySpinner || this.spinner;
        spinner.show();
        return toPromise(this.createRequest(record))
            .then((x) => {
                spinner.hide();

                // Side effects
                record = this.parseCreateResponse(record, x);
                this.notifyCreate(record);

                return record;
            }, (error) => {
                spinner.hide();

                this.handleError(error);

                return error;
            });
    }

    protected adaptorUpdateInput(record: T): Object {
        return record;
    }

    protected updateRequest(record: T): Observable<T> {
        const body = {};
        const tyRecord = this.adaptorUpdateInput(record);
        for (const prop in tyRecord) {
            if (tyRecord.hasOwnProperty(prop)) {
                body[prop] = tyRecord[prop];
            }
        }

        return this.http.put<T>(this.updateUrl(record.id), body, {
            withCredentials: this.withCredentialOnRequest
        });
    }

    updateAsync(record: T, mySpinner: ISpinnerService = null): PromiseLike<any> {

        const spinner = mySpinner || this.spinner;
        spinner.show();
        return toPromise(this.updateRequest(record))
            .then((x) => {
                spinner.hide();

                // Side effects
                record = this.parseUpdateResponse(record, x);
                this.notifyUpdate(record);

                return record;
            }, (error) => {
                spinner.hide();

                // TODO: error handling
                this.handleError(error);

                return error;
            });
    }

}
