import { Injector } from '@angular/core';

import { IRelationalTable } from '@polpware/fe-data';

import { lift } from '@polpware/fe-utilities';

import {
    ISpinnerService
} from '@polpware/ngx-spinner';

import {
    ObservableCurdService,
    IBaseEntity
} from './observable-crud.service';

export {
    IBaseEntity
} from './observable-crud.service';

export abstract class ObservableDuetTableService<T extends IBaseEntity>
    extends ObservableCurdService<T> {

    protected primaryTable: IRelationalTable;
    protected secondaryTable: IRelationalTable;

    constructor(injector: Injector) {
        super(injector);
    }

    private buildPublishData() {
        const models = this.primaryTable.dataProvider().models as Array<any>;
        const data = models.map((x) => x.attributes as T);
        return data;
    }

    protected listenToPrimaryTable() {
        this.primaryTable.dataProvider().on('update', () => {
            console.log('Received pimary table updates');
            const data = this.buildPublishData();
            this.subject.next(data);
        });
    }

    protected publishInitData() {
        const data = this.buildPublishData();
        this.subject.next(data);
    }

    // Override
    protected getListGuard(): boolean {
        return false;
    }

    // Implement
    getById(id: string): T {
        const model = this.primaryTable.get(id);
        if (model) {
            return model.attributes as T;
        }
        return null;
    }

    getByIdAsync(id: string, mySpinner: ISpinnerService = null): PromiseLike<T> {
        throw new Error('Not implemented');
    }

    // Override
    protected deleteByIdGuard(id: string) {
        return false;
    }

    // Override
    protected notifyDelete(id: string) {
        // Side effects
        const model = this.primaryTable.get(id);
        if (model) {
            model.destroyFromTable();
        }
    }

    // Override
    protected notifyCreate(record: T) {
        this.primaryTable.add(record);
    }

    protected notifyUpdate(record: T) {
        // The following op basically update what we have ...
        this.primaryTable.add(record);
    }
}
