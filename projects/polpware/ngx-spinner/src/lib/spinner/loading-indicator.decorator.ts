import { ILoadingIndicator } from '../interfaces/indicators.interface';
import { ISpinnerService } from './spinner.interface';

interface IDecoratorPrerequisite {
    spinner: ISpinnerService;
}

type DecoratorPrequisiteClass = { new(...args: any[]): IDecoratorPrerequisite };

export function loadingIndicatorDecorator<T extends DecoratorPrequisiteClass>(constructor: T) {
    return class extends constructor implements ILoadingIndicator {

        public showLoadingIndicator(...args: any[]) {
            this.spinner.show(...args);
        }

        public hideLoadingIndicator(...args: any[]) {
            this.spinner.hide(...args);
        }

        public setLoadingIndicatorDelay(seconds: number) {
            this.spinner.setDelay(seconds);
        }
    };
}
