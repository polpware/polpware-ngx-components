import { Injectable } from '@angular/core';
import { LoggerProviderImpl } from '@polpware/ngx-logger';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { SpinnerServiceBase } from './spinner-service-base';

const PRIMARY_SPINNER = 'primary';

/* Note that on purpose we do not turn this one into a singular service. 
 * Therefore, we are able to create many such services for each component */

@Injectable()
export class SpinnerServiceImpl extends SpinnerServiceBase {

    private _subr: Subscription;

    constructor(protected readonly underlyingSpinner: NgxSpinnerService,
        loggerProvider: LoggerProviderImpl) {
        super();

        this.logger = loggerProvider.logger('polpware_ngx_spinner');
    }

    // Note that we do not need to stop it, as this is a service starting in the beginning.
    public startToListenSpinner(name: string = PRIMARY_SPINNER) {
        // Set up the listener
        this._subr = this.underlyingSpinner.getSpinner(name).subscribe(x => {
            this.spinnerState = x.show;
        });
    }

    public stopListener(name: string = PRIMARY_SPINNER) {
        this._subr && this._subr.unsubscribe();
    }

}

