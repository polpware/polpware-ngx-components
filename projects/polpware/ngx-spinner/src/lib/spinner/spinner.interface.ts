export interface ISpinnerService {
    show(...args: any[]): void;
    hide(...args: any[]): void;
    setDelay(seconds: number): void;
}

export class NullSpinner implements ISpinnerService {
    show() { }
    hide() { }
    setDelay(seconds: number) { }
}
