/*
 * Public API Surface of ngx-gadgets
 */

export * from './lib/language-switch/language-switch.component';
export * from './lib/ngx-gadgets.module';
