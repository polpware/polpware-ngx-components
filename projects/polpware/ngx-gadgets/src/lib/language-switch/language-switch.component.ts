import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'polp-gadget-language-switch',
    templateUrl: './language-switch.component.html',
    styleUrls: ['./language-switch.component.scss']
})
export class LanguageSwitchComponent implements OnInit, OnChanges {

    faGlobe = faGlobe;

    @Input() langOptions: Array<{
        text: string;
        value: string;
    }> = [];

    @Input() initValue: string = '';
    @Input() size: string = '';

    @Output() changed = new EventEmitter();

    langValue: string;
    isSmallSize: boolean = false;
    isLargeSize: boolean = false;

    constructor() { }

    ngOnInit(): void {
        this.langValue = this.initValue;
        if (this.size == 'small') {
            this.isSmallSize = true;
        } else if (this.size == 'large') {
            this.isLargeSize = true;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.initValue) {
            if (this.initValue != this.langValue) {
                this.langValue = this.initValue;
                // Note that this will not trigger event.
            }
        }
    }

    change($event: any) {
        this.changed.emit(this.langValue);
    }

}
