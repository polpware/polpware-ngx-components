import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LanguageSwitchComponent } from './language-switch/language-switch.component';


@NgModule({
    declarations: [LanguageSwitchComponent],
    imports: [
        CommonModule,
        FormsModule,
        FontAwesomeModule
    ],
    exports: [LanguageSwitchComponent]
})
export class NgxGadgetsModule { }
