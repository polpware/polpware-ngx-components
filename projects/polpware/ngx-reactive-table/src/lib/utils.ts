import { HtmlInputTypeEnum } from './interfaces';

/**
 * Returns a slice of array (with value semantics) [fromIndex, endIndex]
 * @param data
 * @param fromIndex
 * @param endIndex
 */
export function sliceArray(data: Array<any>, fromIndex: number, endIndex: number) {
    return data.filter((_, index) => {
        return index >= fromIndex && index <= endIndex;
    });
}

export function countProperties(obj: Object) {
    let count = 0;
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            ++count;
        }
    }
    return count;
}

export function getInputType(data: any): HtmlInputTypeEnum {
    const ty = typeof data;
    if (ty == 'number') {
        return 'number';
    }
    if (ty == 'boolean') {
        return 'checkbox';
    }
    if (ty == 'object') {
        if (data instanceof Date) {
            return 'date';
        }
    }
    return 'text';
}

export function defaultInputTypeValue(t: HtmlInputTypeEnum) {
    if (t == 'number') {
        return 0;
    }
    if (t == 'checkbox') {
        return false;
    }
    return '';
}
