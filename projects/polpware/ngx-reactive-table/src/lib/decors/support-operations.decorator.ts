import { IRowDataType, ITableWithOperationsBase } from '../interfaces';
import { countProperties, defaultInputTypeValue, sliceArray } from '../utils';

interface IDecoratorPrerequisite<T extends IRowDataType> extends ITableWithOperationsBase<T> {
}

type DecoratorPrerequisiteClass<T extends IRowDataType> = { new(...args: any[]): IDecoratorPrerequisite<T> };

export function supportOperationsDecorator<T extends IRowDataType, U extends DecoratorPrerequisiteClass<T>>(constructor: U) {
    return class extends constructor implements ITableWithOperationsBase<T> {

        get anyEditing() {
            return countProperties(this.backup) > 0;
        }

        onSelect(a: { selected: Array<T> }) {
            if (a && a.selected && Array.isArray(a.selected)) {
                this.selected = [...a.selected];
            }
        }

        startAdd() {
            const newElem: any = {
                id: ''
            };
            this.columns.forEach(a => {
                if (a.editable) {
                    newElem[a.prop] = defaultInputTypeValue(a.inputType);
                }
            });
            // Disable sorting
            this.sorts = [];
            // Add the element into the rows (no backup)
            this.datatable.rows = [newElem, ...this.datatable._internalRows];
            this.totalCount = this.totalCount + 1;
            this.backup[0] = newElem;
            // Enable editing it.
            this.columns.forEach(a => {
                if (a.editable) {
                    this.editing[0 + '-' + a.prop] = true;
                }
            });
        }

        startEdit(rowIndex: number) {
            // Disable sorts
            this.sorts = [];
            const data = this.datatable._internalRows[rowIndex];
            this.backup[rowIndex] = { ...data };
            this.columns.forEach(a => {
                if (a.editable) {
                    this.editing[rowIndex + '-' + a.prop] = true;
                }
            });
        }

        // Support editing an existing one and adding a new one
        cancelEdit(rowIndex: number) {
            // Replace the old value
            const firstPart = sliceArray(this.datatable._internalRows, 0, rowIndex - 1);
            const secondPart = sliceArray(this.datatable._internalRows, rowIndex + 1, this.datatable._internalRows.length - 1);
            const elem = this.backup[rowIndex];
            // An existing one
            if (elem.id) {
                this.rows = [...firstPart, elem, ...secondPart];
            } else {
                // Otherwise, drop this.
                this.rows = [...firstPart, ...secondPart];
            }
            this.cleanEditing(rowIndex);
            delete this.backup[rowIndex];
        }


        async confirmEditAsync(rowIndex: number) {
            try {
                const elem = this.datatable._internalRows[rowIndex];
                let newElem: IRowDataType = elem;
                let op: 'create' | 'update' | '' = '';
                if (elem.id) {
                    op = 'update';
                    // Update an existing elment
                    if (this.settings.updateAsyncHandler) {
                        newElem = await this.settings.updateAsyncHandler(elem);
                    }
                } else {
                    op = 'create';
                    // Update an existing elment
                    if (this.settings.createAsyncHandler) {
                        newElem = await this.settings.createAsyncHandler(elem);
                    }
                }

                // todo: Do we need to update data ????
                const firstPart = sliceArray(this.datatable._internalRows, 0, rowIndex - 1);
                const secondPart = sliceArray(this.datatable._internalRows, rowIndex + 1, this.rows.length - 1);
                this.rows = [...firstPart, newElem, ...secondPart];

                this.cleanEditing(rowIndex);
                delete this.backup[rowIndex];

                this.publish({
                    op: op,
                    data: newElem,
                    rows: this.rows
                });
            } catch (e) {
                this.noty.error('Sorry, something went wrong!', 'Operation result');
            }
        }

        updateValue(event: any, prop: string, rowIndex: number) {
            this.datatable._internalRows[rowIndex][prop] = event.target.value;
        }

        cleanEditing(rowIndex: number) {
            this.columns.forEach(a => {
                if (a.editable) {
                    delete this.editing[rowIndex + '-' + a.prop];
                }
            });
        }

        async rmAsync() {
            try {
                if (this.settings.deleteAsyncHandler) {
                    // Expect to be a transaction 
                    await this.settings.deleteAsyncHandler(this.selected);
                }
                // This operation preserve sorting
                // Therfore, we on purpose use rows instead of internal rows

                // Do not refresh; just delete them from the local set.
                // Update data
                this.rows = this.rows.filter(a => !this.selected.some(b => b === a));
                this.totalCount = this.totalCount - this.selected.length;

                const oldSelected = this.selected;
                this.selected = [];

                this.noty.success('Data has been deleted successfully!', 'Operation result');
                this.publish({
                    op: 'delete',
                    data: oldSelected,
                    rows: this.rows
                });
            } catch (e) {
                this.noty.error('Sorry, something went wrong!', 'Operation result');
            }
        }

    }
}
