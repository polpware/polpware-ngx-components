import { Subscription } from 'rxjs';
import { ISearchCapabiltyBuilder } from '../interfaces';

interface IDecoratorPrerequisite extends ISearchCapabiltyBuilder {
}

type DecoratorPrerequisiteClass = { new(...args: any[]): IDecoratorPrerequisite };

export function hasInlineSearchDecorator<T extends DecoratorPrerequisiteClass>(constructor: T) {
    return class extends constructor implements ISearchCapabiltyBuilder {

        _searchKeywordSubr: Subscription;

        // Start to listen for search keyword change
        startObserveSearchKeyword() {
            this._searchKeywordSubr = this.searchControl.valueChanges.subscribe(a => {
                a = (a || '').toLowerCase();
                this.anyFutureKeyword = a;
                this.kickOffSearch();
            });
        }

        stopObserveSearchKeyword() {
            this._searchKeywordSubr && this._searchKeywordSubr.unsubscribe();
        }

    }
}
