import { TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { INgxNoty } from '@polpware/ngx-noty';
import { DatatableComponent } from '@swimlane/ngx-datatable';

export type HtmlInputTypeEnum = 'text' | 'tel' | 'email' | 'date' | 'number' | 'checkbox' | 'file';

export interface IRowDataType {
    id: any
}

export interface IComponentSettings {
    pageSize?: number;
    canCreate?: boolean;
    canUpdate?: boolean;
    canDelete?: boolean;
    createAsyncHandler?: (data: IRowDataType) => Promise<IRowDataType>;
    deleteAsyncHandler?: (data: Array<IRowDataType>) => Promise<any>;
    updateAsyncHandler?: (data: IRowDataType) => Promise<IRowDataType>;
}

export const noopPromise = (data: any) => new Promise((resolve, reject) => {
    resolve(data);
});

export const deletePromise = (data: Array<IRowDataType>) => new Promise((resolve, reject) => {
    resolve();
});

export const createPromise: (data: IRowDataType) => Promise<IRowDataType> = (data: IRowDataType) => new Promise((resolve, reject) => {
    const newData: IRowDataType = Object.assign({}, data, { id: new Date().getTime() });
    resolve(newData);
});

export const updatePromise: (data: IRowDataType) => Promise<IRowDataType> = (data: IRowDataType) => new Promise((resolve, reject) => {
    const newData: IRowDataType = Object.assign({}, data);
    resolve(newData);
});

export const defaultSettings: IComponentSettings = {
    pageSize: 40,
    canCreate: false,
    canUpdate: false,
    canDelete: false,
    createAsyncHandler: createPromise,
    updateAsyncHandler: updatePromise,
    deleteAsyncHandler: deletePromise
}

export interface ITableColumnSpec {
    name?: string;
    prop?: string;
    width?: number;
    maxWidth?: number;
    minWidth?: number;
    canAutoResize?: boolean;
    draggable?: boolean;
    resizeable?: boolean;
    headerCheckboxable?: boolean;
    checkboxable?: boolean;
    sortable?: boolean;
    headerTemplate?: TemplateRef<any>;
    cellTemplate?: TemplateRef<any>;
    headerClass?: string;
    cellClass?: string;
    editable?: boolean; // editable, not part of the ngx datatable
    inputType?: HtmlInputTypeEnum;
    frozenLeft?: boolean;
    frozenRight?: boolean;
}

export interface ISortableColumn {
    prop: string;
    dir: 'asc' | 'desc';
}

export interface ISortChangedEvent {
    column: {
        prop: string;
    };
    newValue: 'asc' | 'desc';
}

export interface IPageChangedEvent {
    offset: number;
}

export interface IReactiveDatableBase<T extends IRowDataType> {

    datatable: DatatableComponent;

    settings: IComponentSettings;

    columns: Array<ITableColumnSpec>;

    rows: Array<T>;

    pageSize: number;
    loading: boolean;

    totalCount: number;
    pageIndex: number; // page index

    sortingKey: string;
    // Describe the init sorting order.
    sorts: Array<ISortableColumn>;
}

export interface ITableDataChangeEvent {
    op: 'create' | 'delete' | 'update' | '',
    data: Array<IRowDataType> | IRowDataType,
    rows: Array<IRowDataType>
}

export interface ITableWithOperationsBase<T extends IRowDataType> extends IReactiveDatableBase<T> {

    // Support selected
    selected: Array<T>;
    // Suport editing
    editing: { [key: string]: boolean };
    backup: { [key: number]: T };

    anyEditing: boolean;

    noty: INgxNoty;

    publish(event: ITableDataChangeEvent): void;
    onSelect(data: { selected: Array<T> }): void;
    startAdd(): void;
    startEdit(rowIndex: number): void;
    cancelEdit(rowIndex: number): void;
    confirmEditAsync(rowIndex: number): Promise<any>;
    updateValue(event: any, prop: string, rowIndex: number): void;
    cleanEditing(rowIndex: number): void;
    rmAsync(): Promise<any>;
}

export interface ISearchCapabiltyBuilder {
    // Search control input
    searchControl: FormControl;

    //
    // This property tracks if there is any keyword 
    // which may be applied in the future.
    // E.g., though there is a keyword in effect,
    // a user may enter new keyword in the search input control
    // and the new value is not equal to the current effective
    // keyword. In this case, anyFutureKeyword tells the new value. 
    anyFutureKeyword: string;

    // Start to listen for search keyword change
    startObserveSearchKeyword(): void;

    stopObserveSearchKeyword(): void;

    // Cancel typed keyword and
    // reset to whatever the previous state
    //
    // This operation does not cause new network request.
    cancelTypedKeyword(): void;

    // Starts a new round of search
    //
    // This operation causes new network request.
    kickOffSearch(): void;
}

/**
 * pageIndex starting with 0
 */
export interface IGeneralPagedRequest {
    pageSize: number;
    pageIndex: number; // Starting from 0
}

export interface IGeneralPagedResponse<T> {
    totalCount: number;
    items: Array<T>;
}

export interface IAbpPagedRequest {
    skipCount?: number;
    maxResultCount?: number;
}

export interface IAbpPagedAndSortedRequest extends IAbpPagedRequest {
    sorting?: string;
}

export interface IAbpPagedResponse<T> {
    totalCount: number;
    items: Array<T>;
}

export function adaptToGeneralPagedRequest(input: IAbpPagedRequest): IGeneralPagedRequest {
    return {
        pageSize: input.maxResultCount,
        pageIndex: Math.floor(input.skipCount / input.maxResultCount)
    };
}

export function adaptToAbpPagedRequest(input: IGeneralPagedRequest): IAbpPagedRequest {
    return {
        skipCount: input.pageIndex * input.pageSize,
        maxResultCount: input.pageSize
    };
}
