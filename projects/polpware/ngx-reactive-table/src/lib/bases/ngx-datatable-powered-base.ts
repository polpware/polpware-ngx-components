import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { defaultSettings, IComponentSettings, IReactiveDatableBase, IRowDataType, ISortableColumn, ITableColumnSpec } from '../interfaces';

export abstract class NgxDatatablePoweredBase<T extends IRowDataType>
    implements IReactiveDatableBase<T> {

    // We on purpose make this one to be abstract,
    // and thus the dervied must use a viewchild to bind its value.
    abstract datatable: DatatableComponent;

    // Note that we do not make settings be an input,
    // becasue the behavior of an input in the abstract base class is not well-defined
    // Sometime, it works  and sometimes it causes compilation errors.
    settings: IComponentSettings = defaultSettings;

    ColumnMode = ColumnMode;

    columns: Array<ITableColumnSpec> = [];

    rows: Array<T> = [];

    pageSize: number = defaultSettings.pageSize;
    loading: boolean = false;

    totalCount: number = 0;
    pageIndex: number = 0; // page index

    // We on purpose put the basic for sorting here, because they are required in
    // supporting operations. 
    // Sorting 
    sortingKey: string = '';
    // Describe the init sorting order.
    sorts: Array<ISortableColumn> = [];
}
