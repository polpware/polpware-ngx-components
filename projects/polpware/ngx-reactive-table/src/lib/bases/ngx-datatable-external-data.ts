import { INgxNoty } from '@polpware/ngx-noty';
import { SelectionType } from '@swimlane/ngx-datatable';
import { IPageChangedEvent, IRowDataType, ISortChangedEvent, ITableDataChangeEvent, ITableWithOperationsBase, noopPromise } from '../interfaces';
import { NgxDatatablePoweredBase } from './ngx-datatable-powered-base';

export abstract class NgxDatatableExternalData<T extends IRowDataType>
    extends NgxDatatablePoweredBase<T> {

    constructor() {
        super();
    }

    protected abstract buildTableDataAsync(pageIndex: number): Promise<any>;

    onPageChanged(evt: IPageChangedEvent) {
        this.buildTableDataAsync(evt.offset);
    }

    onPageSizeChanged(size: number) {
        this.pageSize = size;
        this.refresh();
    }

    refresh() {
        // The other settings stay the same.
        this.buildTableDataAsync(0);
    }

    abstract onSortChanged(evt: ISortChangedEvent): any;

}

export abstract class NgxDatatableExternalDataWithOperations<T extends IRowDataType>
    extends NgxDatatableExternalData<T>
    implements ITableWithOperationsBase<T> {

    SelectionType = SelectionType;

    // Support selected
    selected: Array<T> = [];
    // Suport editing
    editing: { [key: string]: boolean } = {};
    backup: { [key: number]: T } = {};

    anyEditing: boolean

    abstract noty: INgxNoty;

    constructor() {
        super();
    }

    abstract publish(event: ITableDataChangeEvent): void;

    onSelect({ selected }) { }

    startAdd() { }

    startEdit(rowIndex: number) { }

    // Support editing an existing one and adding a new one
    cancelEdit(rowIndex: number) { };


    async confirmEditAsync(rowIndex: number) {
        await noopPromise(rowIndex);
    }

    updateValue(event: any, prop: string, rowIndex: number) { }

    cleanEditing(rowIndex: number) { }

    async rmAsync() {
        await noopPromise(null);
    }

}
