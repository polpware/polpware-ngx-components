import { FormControl } from '@angular/forms';
import { INgxNoty } from '@polpware/ngx-noty';
import { SelectionType } from '@swimlane/ngx-datatable';
import { IRowDataType, ISearchCapabiltyBuilder, ITableDataChangeEvent, ITableWithOperationsBase, noopPromise } from '../interfaces';
import { NgxDatatablePoweredBase } from './ngx-datatable-powered-base';

// Dervied classes must use decorator to provide implementation
export abstract class NgxDatatableLocalData<T extends IRowDataType>
    extends NgxDatatablePoweredBase<T> implements ITableWithOperationsBase<T> {

    SelectionType = SelectionType;

    // Support selected
    selected: Array<T> = [];
    // Suport editing
    editing: { [key: string]: boolean } = {};
    backup: { [key: number]: T } = {};

    anyEditing: boolean

    abstract noty: INgxNoty;

    constructor() {
        super();
    }

    abstract publish(event: ITableDataChangeEvent): void;

    onSelect({ selected }) { }

    startAdd() { }

    startEdit(rowIndex: number) { }

    // Support editing an existing one and adding a new one
    cancelEdit(rowIndex: number) { };


    async confirmEditAsync(rowIndex: number) {
        await noopPromise(rowIndex);
    }

    updateValue(event: any, prop: string, rowIndex: number) { }

    cleanEditing(rowIndex: number) { }

    async rmAsync() {
        await noopPromise(null);
    }
}



export abstract class NgxDatatableLocalDataWithInlineSearch<T extends IRowDataType>
    extends NgxDatatablePoweredBase<T> implements ITableWithOperationsBase<T>, ISearchCapabiltyBuilder {

    SelectionType = SelectionType;

    // Support selected
    selected: Array<T> = [];
    // Suport editing
    editing: { [key: string]: boolean } = {};
    backup: { [key: number]: T } = {};

    anyEditing: boolean

    abstract noty: INgxNoty;

    // Support for local filters
    abstract searchControl: FormControl;
    anyFutureKeyword: string;


    constructor() {
        super();
    }

    abstract publish(event: ITableDataChangeEvent): void;

    onSelect({ selected }) { }

    startAdd() { }

    startEdit(rowIndex: number) { }

    // Support editing an existing one and adding a new one
    cancelEdit(rowIndex: number) { };


    async confirmEditAsync(rowIndex: number) {
        await noopPromise(rowIndex);
    }

    updateValue(event: any, prop: string, rowIndex: number) { }

    cleanEditing(rowIndex: number) { }

    async rmAsync() {
        await noopPromise(null);
    }

    // Support for local filters
    startObserveSearchKeyword(): void { }
    stopObserveSearchKeyword(): void { }
    abstract cancelTypedKeyword(): void;
    abstract kickOffSearch(): void;

}
