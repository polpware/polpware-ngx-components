import { IAlertProvider, IAlertItem } from './interfaces';

export class AlertDefaultImpl implements IAlertProvider {
    private _items: Array<IAlertItem> = [];

    get data() {
        return this._items;
    }

    clean(): void {
        this._items = [];
    }

    info(m: string, timeout?: number) {
        this._items.push({
            type: 'info',
            message: m,
            timeout: timeout || 0
        });
    }

    success(m: string, timeout?: number) {
        this._items.push({
            type: 'success',
            message: m,
            timeout: timeout || 0
        });
    }

    warning(m: string, timeout?: number) {
        this._items.push({
            type: 'warning',
            message: m,
            timeout: timeout || 0
        });
    }

    danger(m: string, timeout?: number) {
        this._items.push({
            type: 'danger',
            message: m,
            timeout: timeout || 0
        });
    }
}
