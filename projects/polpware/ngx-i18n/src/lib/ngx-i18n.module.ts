import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HyperTranslatePipe } from './services/hyper-translator.pipe';

@NgModule({
    declarations: [HyperTranslatePipe],
    imports: [
        CommonModule,
    ],
    exports: [HyperTranslatePipe]
})
export class NgxI18nModule { }
