import { TestBed } from '@angular/core/testing';

import { NgxTranslatorImplService } from './ngx-translator-impl.service';

describe('NgxTranslatorImplService', () => {
  let service: NgxTranslatorImplService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxTranslatorImplService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
