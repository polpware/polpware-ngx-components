import { ChangeDetectorRef, Injectable, Pipe } from '@angular/core';
import { HyperTranslatePipeBase } from './hyper-translator-pipe-base';
import { NgxTranslatorImplService } from './ngx-translator-impl.service';

@Injectable()
@Pipe({
    name: 'hyperTrans',
    pure: false // required to update the value when the promise is resolved
})
export class HyperTranslatePipe extends HyperTranslatePipeBase {

    constructor(protected _translate: NgxTranslatorImplService, protected _ref: ChangeDetectorRef) {
        super();
    }
}
