import { EventEmitter } from "@angular/core";
import { Observable, of } from "rxjs";
import { DefaultLangChangeEvent, INgxTranslator, LangChangeEvent, TranslationChangeEvent } from '../interfaces/ngx-translator.interface';
import { lookupDeeply } from './utils';


/* On purpose we do not make it injectable. 
   It is up to the host to define how to do this */

export class NgxTranslatorImplService implements INgxTranslator {


    protected _dict: { [key: string]: any } = {};

    onTranslationChange: EventEmitter<TranslationChangeEvent> = new EventEmitter<TranslationChangeEvent>();
    onLangChange: EventEmitter<LangChangeEvent> = new EventEmitter<LangChangeEvent>();
    onDefaultLangChange: EventEmitter<DefaultLangChangeEvent> = new EventEmitter<DefaultLangChangeEvent>();

    defaultLang = '';

    currentLang = '';

    langs = [];

    public getParsedResult(translations: any, key: any, interpolateParams?: Object): any {
        return key;
    }

    public get(key: string | Array<string>, interpolateParams?: Object): Observable<string | any> {
        const v = lookupDeeply(this._dict, key, interpolateParams);
        return of(v);
    }

    public loadResources(resources: { [key: string]: any }) {
        // todo: A better implementation
        Object.assign(this._dict, resources);
    }
}
