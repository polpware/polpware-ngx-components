/*
 * Public API Surface of ngx-events
 */

export * from './lib/services/global-events.service';
export * from './lib/ngx-events.module';
