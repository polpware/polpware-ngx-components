import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface FunctionSignature {
    name: string;
    args: any[];
    callback?: () => void;
}

@Injectable()
export class GlobalEventsService {
    private _listeners: { [key: string]: any };
    private _subject: Subject<FunctionSignature>;

    constructor() {
        this._listeners = {};
        this._subject = new Subject();

        this._subject.asObservable().subscribe(
            (next) => {
                const name = next.name;
                const args = next.args;
                const callback = next.callback;
                if (this._listeners[name]) {
                    for (const listener of this._listeners[name]) {
                        listener(...args);
                    }

                    if (callback) {
                        callback();
                    }
                }
            },
            (error) => {
            }
        );
    }

    on(name: string, listener) {
        if (!this._listeners[name]) {
            this._listeners[name] = [];
        }
        this._listeners[name].push(listener);
    }

    off(name: string, listener?) {
        if (!this._listeners[name]) {
            return;
        }
        if (!listener) {
            delete this._listeners[name];
            return;
        }

        const callbacks = this._listeners[name];
        let anyIndex = -1;
        let index;
        for (index = 0; index < callbacks.length; index++) {
            if (callbacks[index] === listener) {
                anyIndex = index;
                break;
            }
        }
        if (anyIndex !== -1) {
            callbacks.splice(anyIndex, 1);
        }
    }

    broadcast(name: string, args: any[] = [], callback: () => void = null) {
        this._subject.next({
            name: name,
            args: args,
            callback: callback
        });
    }
}
