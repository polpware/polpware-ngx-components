import {
    ModuleWithProviders, NgModule,
    Optional, SkipSelf
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { GlobalEventsService } from './services/global-events.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ]
})
export class PolpNgxEventsModule {
    constructor(@Optional() @SkipSelf() parentModule: PolpNgxEventsModule) {
        if (parentModule) {
            throw new Error(
                'PolpNgxEventsModule is already loaded and please import it in the AppModule only.');
        }
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: PolpNgxEventsModule,
            providers: [GlobalEventsService]
        };
    }
}
