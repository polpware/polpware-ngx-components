import { INgxNoty } from '../interfaces/ngx-noty.interface';

/**
 * We on purpose do not make this class to have a root provider. 
 * So that the application feels free to set up it. 
 */
export class NgxNotyImpl implements INgxNoty {

    success(message: string, title: string, options?: { [key: string]: any }) {
    }

    error(message: string, title: string, options?: { [key: string]: any }) {
    }

    info(message: string, title: string, options?: { [key: string]: any }) {
    }

    warning(message: string, title: string, options?: { [key: string]: any }) {
    }

}
