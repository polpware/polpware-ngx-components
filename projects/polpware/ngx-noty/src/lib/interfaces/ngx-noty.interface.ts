////////////////////////////////////////////////////////////////////////////////
// References
//
//   - [ ] https://github.com/kreuzerk/primeNG-advanced-growl
//   - [ ] https://github.com/artemsky/ng-snotify
//   - [ ] https://github.com/jacob-meacham/angular-notification-icons (notification icon but for angularjs)
//   - [ ] https://github.com/scttcper/ngx-toastr
//
////////////////////////////////////////////////////////////////////////////////


export interface INgxNoty {
    success(message: string, title: string, options?: { [key: string]: any });
    error(message: string, title: string, options?: { [key: string]: any });
    info(message: string, title: string, options?: { [key: string]: any });
    warning(message: string, title: string, options?: { [key: string]: any });
}
