/*
 * Public API Surface of ngx-noty
 */

export * from './lib/interfaces/ngx-noty.interface';
export * from './lib/services/ngx-noty.impl';
