import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'shortenFullpath' })
export class ShortenFullpathPipe implements PipeTransform {

    private shortJustName(value: string, left: number, right: number): string {

        if (value.length <= left + right) {
            return value;
        }

        const index = value.lastIndexOf('.');
        if (index === -1) {
            // no extension
            return value.substring(0, left) + '...' + value.substring(value.length - right);
        }

        // has extension
        const ext = value.substring(index);
        if (ext.length >= left + right) {
            return '...' + ext;
        }

        return value.substring(0, left + right - ext.length) + '...' + ext;
    }

    transform(value: string, maxlen: number): string {
        if (value.length <= maxlen) {
            return value;
        }

        const leftLen = Math.trunc(maxlen / 2);
        const rightLen = maxlen - leftLen;

        const index = value.lastIndexOf('\\');
        if (index === -1) {
            return this.shortJustName(value, leftLen, rightLen);
        } else {
            const r = this.shortJustName(value.substring(index + 1), leftLen, rightLen);
            return '...\\' + r;
        }
    }
}
