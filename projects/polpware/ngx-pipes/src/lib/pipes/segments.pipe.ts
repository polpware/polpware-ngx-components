import { Pipe, PipeTransform } from '@angular/core';

/*
 * Transform a string into an array of segments. 
 * Usage:
 *   path | segments
 * Example:
 *   {{ '\test\tt' |  segments:'\\' }}
 *   formats to: ['test', 'tt']
*/
@Pipe({ name: 'segments' })
export class SegmentsPipe implements PipeTransform {

    transform(path: string, separator: string = '\\'): Array<{ value: string, id: number, lastId: number }> {
        if (!path) {
            return [];
        }

        let ss = path.split(separator);
        ss = ss.filter(x => x.length > 0);

        const len = ss.length;
        const pp = ss.map((y, index) => {
            return {
                value: y,
                id: index,
                lastId: len - 1
            };
        });

        return pp;
    }
}
