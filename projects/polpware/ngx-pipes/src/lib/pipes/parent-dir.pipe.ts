import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'parentDir' })
export class ParentDirPipe implements PipeTransform {

    // Remove the last part of a path.
    transform(value: string): string {

        if (!value) {
            return '';
        }

        const index = value.lastIndexOf('\\');
        if (index === -1) {
            return '';
        } else {
            return value.substring(0, index);
        }
    }
}
