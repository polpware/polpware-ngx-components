import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'unzipObject' })
export class UnzipObjectPipe implements PipeTransform {
    transform(value: Object, ...args: string[]): any[] {
        const keys = [];
        for (const key in value) {
            if (value.hasOwnProperty(key)) {
                keys.push({ key: key, value: value[key] });
            }
        }
        return keys;
    }
}
