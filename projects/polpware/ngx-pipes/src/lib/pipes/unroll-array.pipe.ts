import { Pipe, PipeTransform } from '@angular/core';

interface IPair {
    first: any;
    second: any;
}

interface IUnrollResult {
    pairs: Array<IPair>;
    last: any;
}

@Pipe({ name: 'unrollArray' })
export class UnrollArrayPipe implements PipeTransform {

    transform(value: Array<any>): IUnrollResult {
        const len = value.length;
        const half = Math.floor(len / 2);
        let index = 0;
        const groups = [];
        for (let i = 0; i < half; i++) {
            groups.push({
                first: value[index],
                second: value[index + 1]
            });
            index += 2;
        }

        let last = null;

        if (index < len) {
            last = value[index];
        }

        return {
            pairs: groups,
            last: last
        };
    }
}
