NgxLoggerBuildDist := ./dist/polpware/ngx-logger
NgxLoggerDeployTarget := ./deployment/polpware-ngx-logger

build-ngx-logger:
	echo "Build ..."
	ng build @polpware/ngx-logger
	echo "Build done"

copy-ngx-logger:
	echo "Clean old files ..."
	cd $(NgxLoggerDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxLoggerBuildDist)/* $(NgxLoggerDeployTarget)/
	echo "Copy files done"

push-ngx-logger:
	echo "Find new files ..."
	cd $(NgxLoggerDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxLoggerDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxLoggerDeployTarget) && git push
	echo "Push done"

deploy-ngx-logger: build-ngx-logger copy-ngx-logger push-ngx-logger



.PHONY: build-ngx-logger copy-ngx-logger push-ngx-logger deploy-ngx-logger
