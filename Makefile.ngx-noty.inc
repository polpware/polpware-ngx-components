NgxNotyBuildDist := ./dist/polpware/ngx-noty
NgxNotyDeployTarget := ./deployment/polpware-ngx-noty

build-ngx-noty:
	echo "Build ..."
	ng build @polpware/ngx-noty
	echo "Build done"

copy-ngx-noty:
	echo "Clean old files ..."
	cd $(NgxNotyDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -name ".nojekyll" -prune -o -type f -exec rm {} \;
	echo "Clean old files done"
	echo "Copy files ..."
	cp -r $(NgxNotyBuildDist)/* $(NgxNotyDeployTarget)/
	echo "Copy files done"

push-ngx-noty:
	echo "Find new files ..."
	cd $(NgxNotyDeployTarget) && find . -path ./.git -prune -o -name "README.md" -prune -o -type f -exec git add {} \;
	echo "Fine new files done"
	echo "Commit ..."
	cd $(NgxNotyDeployTarget) && git commit -am "New publish"
	echo "Commit done ..."
	echo "Push ..."
	cd $(NgxNotyDeployTarget) && git push
	echo "Push done"

deploy-ngx-noty: build-ngx-noty copy-ngx-noty push-ngx-noty



.PHONY: build-ngx-noty copy-ngx-noty push-ngx-noty deploy-ngx-noty
